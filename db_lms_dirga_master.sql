-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_lms_dirga
CREATE DATABASE IF NOT EXISTS `db_lms_dirga` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_lms_dirga`;

-- Dumping structure for table db_lms_dirga.m_ema
CREATE TABLE IF NOT EXISTS `m_ema` (
  `id_ema` int(11) NOT NULL AUTO_INCREMENT,
  `peristiwa` text NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id_ema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table db_lms_dirga.m_ema: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_ema` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_ema` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_ema_jawaban
CREATE TABLE IF NOT EXISTS `m_ema_jawaban` (
  `id_ema_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `jawaban` varchar(255) NOT NULL,
  `id_ema_soal` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id_ema_jawaban`),
  KEY `FK_ema_jawaban_soal` (`id_ema_soal`),
  CONSTRAINT `FK_ema_jawaban_soal` FOREIGN KEY (`id_ema_soal`) REFERENCES `m_ema_soal` (`id_ema_soal`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_ema_jawaban: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_ema_jawaban` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_ema_jawaban` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_ema_soal
CREATE TABLE IF NOT EXISTS `m_ema_soal` (
  `id_ema_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_ema` int(11) NOT NULL,
  `soal` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id_ema_soal`),
  KEY `FK_era_soal` (`id_ema`),
  CONSTRAINT `m_ema_soal` FOREIGN KEY (`id_ema`) REFERENCES `m_ema` (`id_ema`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table db_lms_dirga.m_ema_soal: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_ema_soal` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_ema_soal` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_era
CREATE TABLE IF NOT EXISTS `m_era` (
  `id_era` int(11) NOT NULL AUTO_INCREMENT,
  `peristiwa` text NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id_era`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_era: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_era` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_era` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_era_soal
CREATE TABLE IF NOT EXISTS `m_era_soal` (
  `id_era_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_era` int(11) NOT NULL,
  `pernyataaan` varchar(255) NOT NULL,
  `pilihan_min` varchar(255) NOT NULL,
  `pilihan_max` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id_era_soal`),
  KEY `FK_era_soal` (`id_era`),
  CONSTRAINT `FK_era_soal` FOREIGN KEY (`id_era`) REFERENCES `m_era` (`id_era`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_era_soal: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_era_soal` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_era_soal` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_perusahaan
CREATE TABLE IF NOT EXISTS `m_perusahaan` (
  `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_perusahaan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_perusahaan: 0 rows
/*!40000 ALTER TABLE `m_perusahaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_perusahaan` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_peserta
CREATE TABLE IF NOT EXISTS `m_peserta` (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_perusahaan` int(11) DEFAULT NULL,
  `nama_peserta` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_peserta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_peserta: 0 rows
/*!40000 ALTER TABLE `m_peserta` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_peserta` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_type_soal
CREATE TABLE IF NOT EXISTS `m_type_soal` (
  `id_type_soal` int(11) NOT NULL AUTO_INCREMENT,
  `type_soal` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_type_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_type_soal: 0 rows
/*!40000 ALTER TABLE `m_type_soal` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_type_soal` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_user
CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_user_level` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_user: 1 rows
/*!40000 ALTER TABLE `m_user` DISABLE KEYS */;
REPLACE INTO `m_user` (`id_user`, `username`, `password`, `id_user_level`, `status`, `create_date`, `update_date`) VALUES
	(1, 'admin', 'b3aca92c793ee0e9b1a9b0a5f5fc044e05140df3', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `m_user` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.m_user_level
CREATE TABLE IF NOT EXISTS `m_user_level` (
  `id_user_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_user_level`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.m_user_level: 1 rows
/*!40000 ALTER TABLE `m_user_level` DISABLE KEYS */;
REPLACE INTO `m_user_level` (`id_user_level`, `nama_level`) VALUES
	(1, 'Super Administrator');
/*!40000 ALTER TABLE `m_user_level` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.t_perusahaan_ema
CREATE TABLE IF NOT EXISTS `t_perusahaan_ema` (
  `id_perusahaan_ema` int(11) NOT NULL AUTO_INCREMENT,
  `id_perusahaan` int(11) NOT NULL,
  `id_ema` int(11) NOT NULL,
  PRIMARY KEY (`id_perusahaan_ema`),
  KEY `id_perusahaan_ema` (`id_perusahaan`,`id_ema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table db_lms_dirga.t_perusahaan_ema: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_perusahaan_ema` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_perusahaan_ema` ENABLE KEYS */;

-- Dumping structure for table db_lms_dirga.t_perusahaan_era
CREATE TABLE IF NOT EXISTS `t_perusahaan_era` (
  `id_perusahaan_era` int(11) NOT NULL AUTO_INCREMENT,
  `id_perusahaan` int(11) NOT NULL,
  `id_era` int(11) NOT NULL,
  PRIMARY KEY (`id_perusahaan_era`),
  KEY `id_perusahaan_era_id_perusahaan_id_era` (`id_perusahaan`,`id_era`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_lms_dirga.t_perusahaan_era: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_perusahaan_era` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_perusahaan_era` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
