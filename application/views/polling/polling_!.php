<style type="text/css">
.container {
/*    display: -webkit-flex;
    display: flex;
    -webkit-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-align-content: center;*/
    align-content: center;
	display: block;
	position: fixed;
    margin: 0;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}

.title {
	font-family: clarendon blk bt;
	text-align: center;
}

.instruction {
	font-family: Bookman Old Style;
	font-weight: bold;
	font-style: italic;
	color: white;
	margin-bottom: 20px;
}

.jawaban {
	display:block;
	align-content: center;
	
}
.jawaban-text {
	font-family: arial;
	color: white;
	font-weight: bold;
	text-align: center;
	font-size:16px;
	margin-top:10px;
}
.img-polling {
    align-content: center;
	display:block;
	text-align:center;
    display: block;
    margin: 0 auto;
    /*filter: gray; /* IE6-9 */*/
    -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ 
    -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);*/
     
	max-height: 75%;
	max-width: 75%;
}

.img-polling:hover {
  filter: none; /* IE6-9 */
  -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */ 
  opacity: .8;
}

body {
   background-image: url('../assets/images/layout/bg.jpg');
    /*font-family: Lora,'Helvetica Neue',Helvetica,Arial,sans-serif;*/
 
   position: relative;
   width: 100%; /* 4:3 Aspect Ratio */
}
.logo-prov{
    height: 80px;
	padding: 10px;
}
.row_poling_top, .row_poling_bot{
    margin-right: 0px;
    margin-left: 0px;
    /*opacity: .7; 
    border-radius: 15px;*/
}
.row_poling_top{
    background-image: url('../assets/images/layout/header.png');
}
.row_poling_bot{
    background-image: url('../assets/images/layout/footer.png');
    /*background-color: #13B6E2; */
    background-size:cover;
    bottom:0px;
  max-height:90px;
   position: fixed;  
				  width:100%;
}

#pertanyaan {
	font-family: 'Brush Script MT';
	color: white;
}



				.footer{
				  float:left;					  
				  width:100%;
				  margin-bottom:10px;
				}
				
				.footer-logo {
					float:left;
					width:50px;	
margin-left:10px;					
				}
				
				.footer-text {
margin-left: 80px;					
					width:100%!important;
					font-size:16px;
					line-height:30px;
	font-family: arial;
	font-size:18px;
				}
									
				.footer h1 {
					font-size:20px;
					margin:0 0 -5px 0;
					padding:0;
					font-family: arial;
					margin-bottom: 3px;
				}
								
				.footer b h1 {
					font-size:27px;
					margin:0 0 -5px 0;
					padding:0;
					font-weight:bold;
					font-family: arial;
				}
				
				.footer span {
					float:left;
					width:100%;
					margin-top:3px;
					line-height:18px;										
				}
</style>
<header class="row row_poling_top">
   	<!-- <div class="col-md-2 col-sm-2 col-xs-2">

        <img class="img-responsive logo-prov" src="<?php echo base_url('assets/images/luwu_timur_logo.png'); ?>"/>
    </div>-->
        <!-- <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Business Casual</div> -->
        <h1 class="title text-center"><center><?php echo strtoupper("Indeks Kepuasan Masyarakat");?></center></h1>
    <div class="col-md-2 col-sm-2 col-xs-2">
        
    </div>
</header>

<div class="container container-polling">
<h2 class="instruction"><center>Tekan Gambar Untuk Memberi Penilaian</center></h2>
	<div id="row_jawaban" class="jawaban">
        <h1 class="text-center" id="pertanyaan"></h1>
        <input type="hidden" name="id_pertanyaan" id="id_pertanyaan">
    </div>
</div>

<footer class="row row_poling_bot">
<div height=100px></div>
<h1 style="height:100px"></h1>
</footer>
<script type="text/javascript">
    $(document).ready(function() {
        var no = 0;
        // var no_pertanyaan = 1;
        get_pertanyaan(no);
        set_jawaban();
    });
    var no_pertanyaan = 1;

    function next(id_jawaban) {
        var id_pertanyaan = $("#id_pertanyaan").val();
        get_pertanyaan(id_pertanyaan);
        save(id_pertanyaan,id_jawaban);
    }

    function get_pertanyaan(no) {
        $.ajax({ url : '<?php echo base_url('polling/ajax/get_pertanyaan'); ?>',
            type : "GET",
            data : {start : no},
            dataType : "json",
            success : function(data) {
                if (data.status) {                    
                    $("#pertanyaan").html(no_pertanyaan+'. '+data.pertanyaan[0].pertanyaan);
                    $("#id_pertanyaan").val(data.pertanyaan[0].id_pertanyaan);
                    no_pertanyaan++;
                } else {
                    swal({
                      title: "Terima Kasih",
                      text: "Penilaian Anda Membuat Kami Lebih Baik!",
                      type: "success",
                      showCancelButton: false,
                      confirmButtonClass: 'btn-success',
                      confirmButtonText: 'Terima Kasih',
						timer: 5000
                    },
                    function(){
                      window.location.href = "<?php echo base_url('polling/home'); ?>";
                    });                    
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
              alert('Error set data from ajax');
            }
        });
    }

    function set_jawaban() {
        $.ajax({ url : '<?php echo base_url('polling/ajax/set_jawaban'); ?>',
            type : "GET",
            dataType : "json",
            success : function(data) {
                $('#row_jawaban').append(data.jawaban);
            },
            error: function (jqXHR, textStatus, errorThrown){
              alert('Error set data from ajax');
            }
        });
    }

    function save(id_pertanyaan,id_jawaban) {
        $.ajax({ url : '<?php echo base_url('polling/ajax/save_polling'); ?>',
            type : "POST",
            data : {id_pertanyaan : id_pertanyaan, jawaban : id_jawaban},
            dataType : "json",
            success : function(data) {
                
            },
            error: function (jqXHR, textStatus, errorThrown){
              alert('Error set data from ajax');
            }
        });
    }
</script>