<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php if (isset($title)) { echo $title; } ?></title>

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/'); ?>css/business-casual.css" rel="stylesheet">
    <!-- sweetalert -->
    <link href="<?php echo base_url('assets/plugins/sweetalert/'); ?>sweetalert.css" rel="stylesheet">

    <!-- <style media="screen">
        body { min-height: auto; padding-top: 70px; }
    </style> -->
    <!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url('assets/plugins/jQuery/'); ?>jquery-2.2.3.min.js"></script>
	<script src="<?php echo base_url('assets/'); ?>js/bootstrap.min.js"></script>
    <!-- sweetalert -->
    <script src="<?php echo base_url('assets/plugins/sweetalert/'); ?>sweetalert.min.js"></script>
  </head>
<body>
	<?php if (isset($content)) { echo $content; } else { echo "<center><h1>Selamat Datang</h1></center>"; } ?>
</body>
</html>