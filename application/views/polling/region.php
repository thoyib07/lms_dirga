<div class="container">
  
  <div class="jumbotron mt-3">
    <h1>Pilih Kecamatan</h1>
    <select name="kecamatan" id="kecamatan" class="form-control">
    	<option> Pilih Kecamatan </option>
    </select>
  	<button class="btn btn-lg btn-primary btn-block" type="submit" name="kirim" value="kirim" onclick="masuk();">Masuk</button>
  </div>

</div> <!-- /container -->
<script type="text/javascript">
	$(document).ready(function() {
		get_kecamatan();
	});

	function get_kecamatan() {
		var url = "<?php echo base_url('polling/ajax/get_kecamatan'); ?>";
		$.ajax({ url : url,
			type : 'GET',
			dataType : 'JSON',
			success: function(data){
		      $('#kecamatan').append(data.kecamatan);
		  	},
		    error: function (jqXHR, textStatus, errorThrown){
		      alert('Error get data from ajax');
		    }

		});
	}

	function masuk() {
		var kode_kecamatan = $('#kecamatan').val();
		if (kode_kecamatan !== 'Pilih Kecamatan') {			
			$.ajax({ url : '<?php echo base_url('polling/ajax/set_kode_kecamatan'); ?>',
				type : 'POST',
				data : {kode_kecamatan : kode_kecamatan },
				dataType: 'json',
				success : function(data) {
					if (data.success) {
						window.location.href = data.redirect;
					}
				},
			    error: function (jqXHR, textStatus, errorThrown){
			      alert('Error set data from ajax');
			    }
			});
		} else {
			alert('Kecamatan Belum Dipilih!!!');
		}
	}
</script>