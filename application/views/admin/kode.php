<div class="container">

  <!-- Main component for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="page-header">
        <a href="<?php echo base_url().'admin/add/kode'; ?>"><button type="button" name="button" class="btn btn-primary">Tambah Kode</button></a>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($result as $key): ?>
          <tr>
            <td><?php echo $id = $id+1; ?></td>
            <td><?php echo $key->kode; ?></td>
            <td>
              <a href="<?php echo base_url().'admin/edit/kode/'.$key->id; ?>"><button type="button" name="button" class="btn btn-info">Edit</button></a>
              <a href="<?php echo base_url().'admin/delete/kode/'.$key->id; ?>"><button type="button" name="button" class="btn btn-danger">Delete</button></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php echo $this->pagination->create_links();
      echo "<br>Total Data : ".$jumlah; ?>
  </div>

</div> <!-- /container -->
