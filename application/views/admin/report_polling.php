<?php
  header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
  header("Content-Disposition: attachment; filename=Report_polling.xls");  //File name extension was wrong
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Cache-Control: private", false);
//*/
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Report Hasil Polling</title>
  </head>
  <body>
    <p><?php echo "Periode : ".$start_date." - ".$end_date; ?></p>
    <table>
      <thead>
        <th>No</th>
        <th>Nama Wilayah</th>
        <th>Pertanyaan</th>
        <th>Jawaban</th>
        <th>Total Per Jawaban</th>
      </thead>
      <tbody>
        <?php 
          $id=0; 
          foreach ($report as $r) :
            switch ($r->id_jawaban) {
              case '1':
                $style = 'style="background-color: green;"';
                break;
              case '2':
                $style = 'style="background-color: yellow;"';
                break;
              
              default:
                $style = 'style="background-color: red;"';
                break;
            }
        ?>
        <tr <?= $style; ?>>
          <td><?php echo $id = $id+1; ?></td>
          <td><?php echo $r->nama; ?></td>
          <td><?php echo $r->pertanyaan; ?></td>
          <td><?php echo $r->jawaban; ?></td>
          <td><?php echo $r->total_jawaban; ?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </body>
</html>
