
<!DOCTYPE html>
<html>
<?php include 'part/head.php'; ?>
<body class="hold-transition skin-blue sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">

  <?php include 'part/header.php'; ?>

  <?php include 'part/side_menu.php'; ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div id="content" class="content-wrapper">
    <?php if (isset($content)) {
      echo $content;
    } else {
      echo "SISTAD";
    } ?>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <!--
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
    -->
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include 'part/foot_js.php'; ?>
</body>
</html>
