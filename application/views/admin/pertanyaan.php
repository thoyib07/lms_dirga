<section class="content-header">
  <h1>
    <?= strtoupper($title); ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url('admin/pertanyaan'); ?>" ><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
    <li class="active"><?php echo ucwords($page['c']); ?></li>
  </ol>
</section>

<section class="content">

  <div class="box">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" title="Daftar Pertanyaan Aktif">Daftar Pertanyaan Aktif</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true" title="Daftar Pertanyaan Tidak Aktif">Daftar Pertanyaan Tidak Aktif</a></li>
      </ul>
      <div class="tab-content">
        <!-- /.tab-pane -->
        <div class="tab-pane active" id="tab_1">
          <button type="button" class="btn btn-success fa fa-plus" onclick="add()" title="Tambah Data"> Tambah</button>
          <button type="button" class="btn btn-primary fa fa-refresh" onclick="reload_table()" title="Refresh Table"></button>
          <table id="list_pertanyaan" class="table table-bordered text-center" style="width: 100% !important;">
            <thead>
                <tr>
                  <td>No</td>
                  <td>Pertanyaan</td>
                  <td>Perintah</td>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <td>No</td>
                  <td>Pertanyaan</td>
                  <td>Perintah</td>
                </tr>
              </tfoot>
          </table>
        </div>

        <div class="tab-pane" id="tab_2">
          <button type="button" class="btn btn-primary fa fa-refresh" onclick="reload_table()" title="Refresh Table"></button>
          <table id="list_del_pertanyaan" class="table table-bordered text-center" style="width: 100% !important;">
            <thead>
              <tr>
                  <td>No</td>
                  <td>Pertanyaan</td>
                  <td>Perintah</td>
              </tr>
            </thead>
            <tfoot>
              <tr>
                  <td>No</td>
                  <td>Pertanyaan</td>
                  <td>Perintah</td>
              </tr>
            </tfoot>
          </table>
        </div>     
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_form">
    <div class="modal-dialog">
      <div class="modal-content">
      <form method="post" id="form" class="form-horizontal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Default Modal</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" value="" name="id_pertanyaan" id="id_pertanyaan">
          <input type="hidden" value="pertanyaan" name="type" id="type">

          <div class="form-group">
            <div class="col-md-12">
              <label>Pertanyaan :</label>
              <input type="text" name="pertanyaan" id="pertanyaan" class="form-control" required style="width:100%">
              <span class="help-block"></span>
            </div>
          </div>

          <input type="hidden" name="submit" id="submit" value="submit">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
          <button type="button" id="btnSave" onclick="save();" class="btn btn-success">Simpan</button>
          <!-- <input type="submit" name="submit" value="Simpan"> -->
        </div>

      </form>

      </div>
    </div>
  </div>

</section>

<script type="text/javascript">
var table1;
var table2;

$(document).ready(function() {
  //datatables
  table1 = $('#list_pertanyaan').DataTable({ 

    "processing"  : true, //Feature control the processing indicator.
    "serverSide"  : true, //Feature control DataTables' server-side processing mode.
    "searchDelay" : 0.5 * 1000,
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "<?php echo base_url('admin/ajax_list?type=list_pertanyaan')?>",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 2 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });

  table2 = $('#list_del_pertanyaan').DataTable({ 

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "searchDelay" : 0.5 * 1000,
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "<?php echo base_url('admin/ajax_list?type=list_del_pertanyaan')?>",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 2 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });

  $('#pertanyaan').change(function() {
    $(this).parent().removeClass('has-error');
    $(this).next().empty();
  });

});

function reload_table(){
  table1.ajax.reload(null,false);
  table2.ajax.reload(null,false);
}

function add() {
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.col-md-12').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string

  $('#btnSave').text('simpan'); //change button text
  $('#btnSave').attr('disabled',false); //set button enable

  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Tambah'); // Set Title to Bootstrap modal title
}

function edit(id) {
  save_method = 'edit';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.col-md-12').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string

  $('#btnSave').text('simpan'); //change button text
  $('#btnSave').attr('disabled',false); //set button enable
  $.ajax({ url : '<?php echo base_url('admin/ajax_get_data/'); ?>'+id,
    type : 'GET',
    data : {type : 'pertanyaan'},
    dataType : 'JSON',
    success : function(data) {
      $('#id_pertanyaan').val(data.detail[0].id_pertanyaan);
      $('#pertanyaan').val(data.detail[0].pertanyaan);

      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah'); // Set Title to Bootstrap modal title
    },
    error : function () {
      alert('Error Get data');
      $('#btnSave').text('simpan'); //change button text
      $('#btnSave').attr('disabled',false); //set button enable
    },
  });
}

function del(id) {
  $.ajax({ url : "<?php echo site_url('admin/ajax_del/')?>"+id,
    type: "GET",
    data : {table : 'm_pertanyaan', field : 'id_pertanyaan'},
    dataType: "JSON",
    beforeSend:function(){
       return confirm("Apakah yakin data ingin dihapus?");
    },
    success: function(data){
        reload_table();
    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Error deleting data');
    }
  });
}

function restore(id) {
  $.ajax({ url : "<?php echo site_url('admin/ajax_restore/')?>"+id,
    type: "GET",
    data : {table : 'm_pertanyaan', field : 'id_pertanyaan'},
    dataType: "JSON",
    beforeSend:function(){
       return confirm("Apakah yakin data ingin dikembalikan?");
    },
    success: function(data){
        reload_table();
    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Error deleting data');
    }
  });
}

function save(){
  $('#btnSave').text('sedang menyimpan...'); //change button text
  $('#btnSave').attr('disabled',true); //set button disable
  var url;

  if(save_method == 'add') {
      // document.getElementById("form").action = "<?php // echo base_url('admin/user/tambah'); ?>"; 
      url = "<?php echo base_url('admin/ajax_insert'); ?>";
  }else if(save_method == 'edit'){
      // document.getElementById("form").action = "<?php // echo base_url('admin/user/ubah'); ?>"; 
      url = "<?php echo base_url('admin/ajax_update/'); ?>"+$('#id_pertanyaan').val();
  }

  $.ajax({ url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data){
        if (data.status) {
          $('#modal_form').modal('hide');
          reload_table();
        } else {
          $('[name="pertanyaan"]').parent().addClass(data.error_class['pertanyaan']);
          $('[name="pertanyaan"]').next().text(data.error_string['pertanyaan']);
        }
        $('#btnSave').text('simpan'); //change button text
        $('#btnSave').attr('disabled',false); //set button enable
      },
      error: function (jqXHR, textStatus, errorThrown){
        if (save_method == 'add') {
          alert('Error adding data');
        } else if(save_method == 'edit') {
          alert('Error update data');
        }
        $('#btnSave').text('simpan'); //change button text
        $('#btnSave').attr('disabled',false); //set button enable
      }
  });
}
</script>