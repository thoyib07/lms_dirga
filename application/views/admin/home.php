<style type="text/css">
  .info-count {
    margin-left: 0px !important;
  }
</style>

<section class="content">

  <div class="box">

    <div class="box-header with-border">
      <h3 class="box-title">Admin Indeks Kepuasan Masyarakat Luwu Timur</h3>
    </div>
    <div class="box-body">
      Selamat Datang Di Kanal Admin Indeks Kepuasan Masyarakat Luwu Timur
    </div>

  </div>

  <div class="row">

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="info-box bg-green">
        <div class="info-box-content info-count">
          <span class="info-box-text">Memuaskan</span>
          <span class="info-box-number"><?= $smile; ?></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="info-box bg-yellow">
        <div class="info-box-content info-count">
          <span class="info-box-text">Biasa Saja</span>
          <span class="info-box-number"><?= $flat; ?></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="info-box bg-red">
        <div class="info-box-content info-count">
          <span class="info-box-text">Kurang Memuaskan</span>
          <span class="info-box-number"><?= $sad; ?></span>
        </div>
      </div>
    </div>

  </div>

  <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Unduh Report</h3>
        </div>
        <div class="box-body">
          <form id="form_report" class="form-horizontal">

            <!-- <div class="col-md-4 col-sm-4 col-xs-12">
              <label>Kecamatan :</label>
              <select name="kecamatan" id="kecamatan" class="form-control" style="width:100%">
                <option value="73.24-LUWU TIMUR"> LUWU TIMUR </option>
              </select>
            </div> -->
          
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label>Tanggal Awal Periode Polling :</label>
              <input type="text" name="start_date" id="start_date" class="form-control datepicker" style="width:100%">
            </div>
          
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label>Tanggal Akhir Periode Polling :</label>
              <input type="text" name="end_date" id="end_date" class="form-control datepicker" style="width:100%">
            </div>

          </form>

          <div class="clearfix"></div><br>     
          <div class="col-md-4 col-sm-4 col-xs-12">
            <button name="download" id="download" onclick="report();" class="form-control btn btn-success" style="width:100%">Download</button>
          </div>
        </div>
      </div>
    </div>

  </div>

</section>

<script type="text/javascript">
  $(document).ready(function() {
    // get_kecamatan();
  });

  // function get_kecamatan() {
  //   var url = "<?php echo base_url('polling/ajax/get_kecamatan'); ?>";
  //   $.ajax({ url : url,
  //     type : 'GET',
  //     dataType : 'JSON',
  //     success: function(data){
  //         $('#kecamatan').append(data.kecamatan);
  //       },
  //       error: function (jqXHR, textStatus, errorThrown){
  //         swal("Error!", "Error get data", "error");
  //       }

  //   });
  // }

  function report() {
    var data = $('#form_report').serialize();
    // console.log(data);
    $.ajax({ url : "<?php echo base_url('admin/ajax_create_report'); ?>",
      type : "POST",
      data : data,
      dataType : "JSON",
      success : function(data) {
        // console.log(data);
        // window.location.href = data;
        window.open(data,'_blank');
      },
      error : function() {
        swal("Error!", "Error get data", "error");
      }
    });
  }
</script>