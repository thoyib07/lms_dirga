<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo (!$title) ? 'SISTAD' : $title; ?> </title>
  <link rel="shortcut icon" href="<?php echo base_url('assets/img/'); ?>favicon.ico" type="image/x-icon" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>plugins/font-awesome/css/font-awesome.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>plugins/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- dataTables -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>plugins/datatables/dataTables.bootstrap.css">
  <!-- dataTables -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/'); ?>plugins/datepicker/datepicker3.css">
  <!-- sweetalert -->
  <link href="<?php echo base_url('assets/plugins/sweetalert/'); ?>sweetalert.css" rel="stylesheet">

  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url('assets/admin/'); ?>plugins/jQuery/jquery-2.1.1.js"></script>

  <!-- dataTables -->
  <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/admin/'); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/admin/'); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

  <!-- dataTables -->
  <script src="<?php echo base_url('assets/admin/'); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- sweetalert -->
  <script src="<?php echo base_url('assets/plugins/sweetalert/'); ?>sweetalert.min.js"></script>
</head>