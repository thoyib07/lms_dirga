<!DOCTYPE html>
<html lang="en">
  <?php include 'parts/head.php'; ?>
  <body>
  	<?php
      include 'parts/menu.php';
      include 'parts/about.php';
      include 'parts/how_to.php';
      /* cek jika sudah login maka login & register hilang
      if ($code == FALSE) {
        # code...
      }
      */
      if ($this->session->userdata['login'] == 'masuk') {
        if ($this->session->userdata['valid_kode'] !== 'valid') {
          include 'parts/code.php';
        }
      } else {
        include 'parts/login.php';
        include 'parts/register.php';
      }

      if ($this->session->userdata['valid_kode'] == 'valid') {
        include 'parts/upload.php';
      }

      include 'parts/gallery.php';
      include 'parts/foot.php';
    ?>
  </body>
</html>
