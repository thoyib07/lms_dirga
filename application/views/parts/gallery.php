<section id="section-07" class="section-07">
  <?php
  $description = urlencode('Sahabat, yuk ikutan lomba foto #NOVAVERSARY29. Caranya kunjungi http://nova.id/ dan menangkan hadiahnya!!');
  $name_share = urlencode('NOVAVERSARY29');
  $ref = urlencode(base_url());
  $img = urlencode(base_url().'assets/images/NOVA.png');
   ?>
  <div>
    <div class="container">
        <h2>Gallery</h2>
        <!-- Start Looping -->
        <?php $i=1; foreach ($result as $key): ?>
          <div class="col-md-3 col-sm-6 animated wow fadeIn">
            <div class="gallery-img" id="<?php echo "gallery-img".$i; ?>" >
              <img src="<?php echo base_url().'assets/uploads/images/'.$key->image; ?>" alt="" class="">
                  <div class="overlay">
                    <a href="http://www.facebook.com/dialog/feed?app_id=1697488337160217&link=<?php echo $ref;?>&picture=<?php echo $img;?>&name=<?php echo $name_share; ?>&description=<?php echo $description; ?>&message=Facebook%20Dialogs%20are%20so%20easy!&redirect_uri=http://facebook.com/" target="_blank"><img src="<?php echo base_url().'assets/'; ?>images/fb-icon.png" alt="facebook"></a>
                    <a href="#" id="<?php echo "pop".$i; ?>" data-toggle="modal" data-target="#image-modal">Detail</a>
                    <a href="https://twitter.com/intent/tweet?original_referer=<?php echo $ref; ?>&text=<?php echo $description; ?>" target="_blank"><img src="<?php echo base_url().'assets/'; ?>images/tw-icon.png" alt="twitter"></a>
                  </div>
              </div>
          </div>

          <div class="modal fade" id="<?php echo "imagemodal".$i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <div class="detail-img"><img src="" class="imagepreview" style="width: 100%;"></div>
                  <div class="description">
                  	<hr>
                      <div class="inner-description clearfix">
                        <div class="author col-xs-5 col-md-7">
                      		<p><?php echo $key->nama; ?></p>
                        </div>
                          <div class="author col-xs-5 col-md-4">
                          	<p>Share : </p>
                      		    <a href="http://www.facebook.com/dialog/feed?app_id=1697488337160217&link=<?php echo $ref;?>&picture=<?php echo $img;?>&name=<?php echo $name_share; ?>&description=<?php echo $description; ?>&message=Facebook%20Dialogs%20are%20so%20easy!&redirect_uri=http://facebook.com/" target="_blank"><img src="<?php echo base_url().'assets/'; ?>images/fb-ico.png" alt="" class="img-responsive"></a>
                              <a href="https://twitter.com/intent/tweet?original_referer=<?php echo $ref; ?>&text=<?php echo $description; ?>" target="_blank"><img src="<?php echo base_url().'assets/'; ?>images/tw-ico.png" alt="" class="img-responsive"></a>
                          </div>
                      </div>
                      <hr>
                  	<p class="story">
                      	<?php echo $key->story; ?>
                      </p>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <script>
        		$(function() {
        				$('#<?php echo "pop".$i; ?>').on('click', function() {
        					$('.imagepreview').attr('src', $('#<?php echo "gallery-img".$i; ?>').find('img').attr('src'));
        					$('#<?php echo "imagemodal".$i; ?>').modal('show');
        				});
        		});
        	</script>
        <?php $i++; endforeach; ?>
        <!-- End Looping -->
        <div class="clearfix"></div>
        <?php echo $this->pagination->create_links();
          echo "<br>Total Data : ".$jumlah; ?>
      </div>
  </div>
</section>
