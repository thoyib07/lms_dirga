<section id="section-03" class="section-03">
  <div>
    	<div class="container">
        	<div class="text-center animated wow fadeInRight">
            	<p>Silakan log in dengan username dan password yang sudah Anda daftarkan</p>
            </div>
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
              <form action="<?php echo base_url().'home/login' ?>" method="post">
                <input type="text" name="username" placeholder="username" class="animated wow fadeInUp" required>
                <input type="password" name="password" placeholder="password" class="animated wow fadeInUp" required>
                <button type="submit"><img src="<?php echo base_url().'assets/'; ?>images/btn-login.png" alt="LOGIN" class="img-responsive animated wow fadeInUp"></button>
              </form>
            </div>
            <div class="text-center col-sm-12 col-md-12 animated wow fadeInUp">
                <p>Belum daftar? Silakan register terlebih dahulu </p>
                <a href="#section-04"><img src="<?php echo base_url().'assets/'; ?>images/btn-register.png" alt="LOGIN" class="img-responsive"></a>
            </div>
        </div>
    </div>
</section>
