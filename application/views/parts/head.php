<head>
  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NOVAVERSARY29</title>
  <meta name="keywords" content="event">
  <!-- Metadata Tiap Page -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="google-site-verification" content="639pG9xOE4lEGGdLtdwdwJrMawDG_8PVn3H7OsujzEY" />
  <meta name="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Content-language" content="id" />
  <meta content="index,follow" name="robots" />
  <meta name="googlebot-news" content="index,follow" />
  <meta name="googlebot" content="all" />
  <meta name="webcrawlers" content="all" />
  <meta name="rating" content="general" />
  <meta name="spiders" content="all" />

  <link itemprop="mainEntityOfPage" href="http://nova.id"/>
  <link rel="canonical" href="http://nova.id" />
  <link rel="publisher" href="https://plus.google.com/105185263312112398412"><!-- G+ account -->
  <link rel="author" href="https://plus.google.com/105185263312112398412"> <!-- G+ account -->
  <link rel="image_src" href="http://assets.nova.id/assets/images/logoNova.png" />
  <link type="image/x-icon" href="http://assets.nova.id/assets/images/favicon.ico" rel="Shortcut icon"/>
  <link type="image/gif" href="http://assets.nova.id/assets/images/favicon.gif" rel="Shortcut icon"/>
  <meta name="theme-color" content="#d9306b">
  <link type="image/x-icon" rel="icon" sizes="192x192" href="http://assets.nova.id/assets/images/favicon.ico">
  <link rel="shortcut icon" href="http://assets.nova.id/assets/images/favicon.ico"/>

  <meta http-equiv="refresh" content="800" />
  <meta name="title" itemprop="name" content="Nova.id Situs Wanita Paling Lengkap" />
  <meta name="author" itemprop="author" content="Tabloid Nova" />
  <meta name="copyright" content="Tabloid Nova" />
  <meta name="description" itemprop="description" content="Nova.id, situs wanita paling lengkap yang menyajikan berita terkini seputar dunia wanita, busana, kecantikan, kuliner sedap, selebriti, kesehatan, profil, keluarga, karier, griya, zodiak horoskop secara inspriatif" />
  <meta name="keywords" content="Situs Wanita, Portal Wanita, Berita Wanita, Dunia Wanita, Tabloid Wanita, Nova, Majalah Nova" />
  <meta name="DC.date.issued" itemprop="datePublished" content="2017-02-15">
  <meta name="MSSmartTagsPreventParsing" content="TRUE" />
  <meta name="generator" content="" />
  <meta name="geo.position" content="-5;120" />
  <meta name="geo.country" content="ID" />
  <meta name="geo.placename" content="Indonesia" />
  <meta itemprop="image" content="http://assets.nova.id/assets/images/logoNova.png" >
  <meta name="news_keywords" content="Situs Wanita, Portal Wanita, Berita Wanita, Dunia Wanita, Tabloid Wanita, Nova, Majalah Nova">
  <meta name="standout" content="http://nova.id"/>
  <meta name="adx:sections" content="Home" />

  <!-- facebook META -->
  <meta property="fb:app_id" content="1415570388756534"/>
  <meta property="fb:pages" content="61561355863" />
  <meta property="og:site_name" content="nova.id" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="http://nova.id" />
  <meta property="og:title" content="Nova.id Situs Wanita Paling Lengkap" />
  <meta property="article:author" content="https://www.facebook.com/wwwtabloidnovacom" />
  <meta property="og:description" content="Nova.id, situs wanita paling lengkap yang menyajikan berita terkini seputar dunia wanita, busana, kecantikan, kuliner sedap, selebriti, kesehatan, profil, keluarga, karier, griya, zodiak horoskop secara inspriatif" />
  <meta property="og:image" content="http://assets.nova.id/assets/images/logoNova.png" />

  <!--twitter cards -->
  <meta name="twitter:card" content="summary_large_image"  data-dynamic="true">
  <meta name="twitter:site" content="@Tabloidnova"  data-dynamic="true">
  <meta name="twitter:title" content="Nova.id Situs Wanita Paling Lengkap"  data-dynamic="true">
  <meta name="twitter:description" content="Nova.id, situs wanita paling lengkap yang menyajikan berita terkini seputar dunia wanita, busana, kecantikan, kuliner sedap, selebriti, kesehatan, profil, keluarga, karier, griya, zodiak horoskop secara inspriatif"  data-dynamic="true">
  <meta name="twitter:creator" content="@Tabloidnova"  data-dynamic="true">
  <meta name="twitter:image" content="http://assets.nova.id/assets/images/logoNova.png"  data-dynamic="true">
  <meta name="twitter:url" content="http://nova.id"  data-dynamic="true">
  <meta name="twitter:domain" content="http://nova.id/"  data-dynamic="true">
  <meta name="alexaVerifyID" content="g9h2mriCdw_xrxrAX3CDaNbGb5w"/>

  <!-- Favicons
  ================================================== -->
  <link rel="shortcut icon" href="<?php echo base_url().'assets/'; ?>img/favicon.png" type="image/x-icon">
  <link rel="apple-touch-icon" href="<?php echo base_url().'assets/'; ?>img/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url().'assets/'; ?>img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url().'assets/'; ?>img/apple-touch-icon-114x114.png">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url().'assets/'; ?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url().'assets/'; ?>css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>fonts/font-awesome/css/font-awesome.css">
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url().'assets/'; ?>css/animate.css">

  <!-- Slider
  ================================================== -->

  <!-- Stylesheet
  ================================================== -->
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url().'assets/'; ?>css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url().'assets/'; ?>js/jquery.1.11.1.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script type="text/javascript" src="<?php echo base_url().'assets/'; ?>js/bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'assets/'; ?>js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'assets/'; ?>js/SmoothScroll.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'assets/'; ?>js/wow.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-3971242-1', 'auto');
    ga('send', 'pageview');
  </script>

  <script>
  $(document).on('ready', function() {
    $("#input-4").fileinput({showCaption: false});
  });
</script>

<script>
  wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 250, // default
    mobile: true, // default
    live: true // default
  })
  wow.init();
</script>

<script>
	$(document).ready(function() {
	$('.nav a').bind("click", function(e) {
	var target = $(this).attr("href"); // Get the target element
	var scrollToPosition = $(target).offset().top; // Position to scroll to
	$('html /* For FF & IE */,body /* For Chrome */').animate({
			'scrollTop': scrollToPosition
		}, 800, function(target){
		window.location.hash = target;
		});
		e.preventDefault();
		});
		});
</script>

<?php if ($this->session->userdata('regis')): ?>
<script type="text/javascript">
  $(window).load(function(){
      $('#myRegis').modal('show');
  });
</script>
<?php $this->session->unset_userdata('regis'); endif; ?>

<?php if (($this->session->userdata('valid_kode') == "tidak valid")||($this->session->userdata('valid_kode') == "kode sudah pernah digunakan")): ?>
<script type="text/javascript">
  $(window).load(function(){
      $('#myCode').modal('show');
  });
</script>
<?php $this->session->set_userdata('valid_kode',FALSE); endif; ?>

</head>
