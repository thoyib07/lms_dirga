<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $table = array('user' => 'user');
    var $column_order = array(null, 'judul'); //set column field database for datatable orderable
    var $column_search = array('judul'); //set column field database for datatable searchable 
    var $order = array('id_user' => 'desc'); // default order 

	public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($type=null,$id=null) {

        switch ($type) {
            case 'user':
            case 'del_user':
                $this->db->select('u.*,s.nama AS SKPD, ut.jabatan');
                $this->db->from('m_user as u');
                $this->db->join('m_skpd as s','u.kode_unor like CONCAT( s.kode_unor, "%")');
                $this->db->join('m_user_type as ut','u.id_user_type = ut.id_user_type');
                break;

            case 'polling':
                $this->db->select('*');
                $this->db->from('t_polling');
                break;

            case 'list_pertanyaan':
            case 'list_del_pertanyaan':
                $this->db->select('*');
                $this->db->from('m_pertanyaan');
                $order_req = false;
                break;
            
            default:
                # code...
                break;
        }
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if (isset($_POST['search'])) {
                if($_POST['search']['value']) // if datatable send POST for search
                {                     
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
     
                    if(count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
        }
        
        if (!isset($order_req)) {
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }  
    }
 
    public function get_datatables($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where('u.status',1);
                break;
            case 'del_user':
                $this->db->where('u.status',0);
                break;
            case 'list_pertanyaan':
                $this->db->where('status',1);
                break;
            case 'list_del_pertanyaan':
                $this->db->where('status',0);
                break;
            
            default:
                # code...
                break;
        }

        if($_POST['length'] != -1){
        	$this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where('u.status',1);
                break;
            case 'del_user':
                $this->db->where('u.status',0);
                break;
            case 'list_pertanyaan':
                $this->db->where('status',1);
                break;
            case 'list_del_pertanyaan':
                $this->db->where('status',0);
                break;
            
            default:
                # code...
                break;
        }

        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where('u.status',1);
                break;
            case 'del_user':
                $this->db->where('u.status',0);
                break;
            case 'list_pertanyaan':
                $this->db->where('status',1);
                break;
            case 'list_del_pertanyaan':
                $this->db->where('status',0);
                break;
            
            default:
                # code...
                break;
        }

        return $this->db->count_all_results();
    }
	
	public function getLimit($value) {
		//var_dump($value);die();
		$this->db->select($value['select']);
		$this->db->from($value['table']);
		$this->db->where($value['where']);
		$this->db->limit($value['limit']);
		$result = $this->db->get()->result();
		return $result;
	}

    public function getAll($value=null) {
        // var_dump("<hr>",$value,"<hr>");
        $this->db->select($value['select']);
        $this->db->from($value['table']);

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        // if (isset($value['where'])) {
        //     foreach ($value['where'] as $where) {
        //         $this->db->where($where);
        //     }
        // }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }
            
        if (isset($value['limit'])) {
            $this->db->limit($value['limit']['0'],$value['limit']['1']);
        }

        if (isset($value['group'])) {
            foreach ($value['group'] as $group) {
                $this->db->group_by($group);
            }
        }
        
        if (isset($value['order'])) {
            $this->db->order_by($value['order']);
        }
        
        $result = $this->db->get()->result();
        return $result;
    }

    public function count_getAll($value=null) {
        $this->db->select($value['select']);
        $this->db->from($value['table']);

        if (isset($value['where'])) {
            $this->db->where($value['where']);
        }

        // if (isset($value['where'])) {
        //     foreach ($value['where'] as $where) {
        //         $this->db->where($where);
        //     }
        // }

        if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }
            
        if (isset($value['limit'])) {
                $this->db->limit($value['limit']['0'],$value['limit']['1']);
        }

        if (isset($value['group'])) {
            foreach ($value['group'] as $group) {
                $this->db->group_by($group);
            }
        }
        
        if (isset($value['order'])) {
            $this->db->order_by($value['order']);
        }
        
        $result = $this->db->get()->num_rows();
        return $result;
    }

  	public function addData($value=null) {
		$this->db->insert($value['table'],$value['data']);
 		$id = $this->db->insert_id();
 		return $id;
  	}
    
    public function updateData($value) {
        if (isset($value['where'])) {
            foreach ($value['where'] as $where) {
                $this->db->where($where['0'],$where['1']);
            }
        }
        $this->db->set($value['data']);
        $this->db->update($value['table']);
    }

    public function delById($value) {
        $data = array('status' => '0',);

        $this->db->where($value['field'], $value['id']);
        $this->db->update($value['table'], $data);
    }

    public function restorById($value) {
        $data = array('status' => '1',);

        $this->db->where($value['field'], $value['id']);
        $this->db->update($value['table'], $data);
    }
	
	function getAllJawaban() {
		$sql = "SELECT * FROM m_jawaban WHERE status=1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getAllPertanyaan() {
		$sql = "SELECT * FROM m_pertanyaan WHERE status=1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getPolling($kode_wilayah='', $id_pertanyaan, $id_jawaban) {
		$where_kode_wilayah = '';
		if ($kode_wilayah!='' && $kode_wilayah!='0') {
			$where_kode_wilayah = " kode_kecamatan='".$kode_wilayah."' AND ";
		}
		$sql = "SELECT id_polling FROM t_polling WHERE ".$where_kode_wilayah." id_pertanyaan='".$id_pertanyaan."' AND id_jawaban='".$id_jawaban."' AND status=1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
