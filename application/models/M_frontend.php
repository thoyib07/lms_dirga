<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_frontend extends CI_Model {

	public function getAll($value) {
		$this->db->select($value['select']);
		$this->db->from($value['from']);

		if (isset($value['where'])) {
			$this->db->where($value['where']);
		}

		if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($value['limit'])) {
			$this->db->limit($value['limit']);
		}

        if (isset($value['order'])) {
        	foreach ($value['order'] as $order) {
                $this->db->order_by($order['0'],$order['1']);
            }			
    		// $this->db->order_by($value['order']['0'],$value['order']['1']);
		}		
		
		$result = $this->db->get()->result();
		return $result;
	}

	public function c_getAll($value) {
		$this->db->select($value['select']);
		$this->db->from($value['from']);

		if (isset($value['where'])) {
			$this->db->where($value['where']);
		}

		if (isset($value['join'])) {
            foreach ($value['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($value['limit'])) {
			$this->db->limit($value['limit']);
		}

        if (isset($value['order'])) {
        	foreach ($value['order'] as $order) {
                $this->db->order_by($order['0'],$order['1']);
            }			
    		// $this->db->order_by($value['order']['0'],$value['order']['1']);
		}		
		
		$result = $this->db->get()->num_rows();
		return $result;
	}

  	public function addData($value=null) {
		$this->db->insert($value['from'],$value['data']);
 		$id = $this->db->insert_id();
 		return $id;
  	}
}