<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Polling extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	function __construct(){

    	parent::__construct();

  	}



	public function index() {

		$data['page'] 		= 'region';

		$data['title'] 		= 'Polling Kepuasan Pelanggan';

		$data['content'] 	= $this->load->view('polling/region',$data,TRUE);

		$this->load->view('polling/layout',$data);		

	}



	public function home() {

		$data_kecamatan = "";

		switch ($_SERVER['HTTP_HOST']) {

			case 'mangkutana.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.1-MANGKUTANA";

				break;

    		case 'nuha.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.2-NUHA";

    			break;

			case 'towuti.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.3-TOWUTI";

				break;

    		case 'malili.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.4-MALILI";

    			break;

			case 'angkona.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.5-ANGKONA";

				break;

    		case 'wotu.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.6-WOTU";

    			break;

			case 'burau.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.7-BURAU";

				break;

    		case 'tomoni.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.8-TOMONI";

    			break;

			case 'kalaena.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.9-KALAENA";

				break;

    		case 'tomoni-timur.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.10-TOMONI TIMUR";

    			break;

    		case 'wasuponda.ikmdukcapilluwutimurkab.com':

				$data_kecamatan = "73.24.11-WASUPONDA";

    			break;
			

			default:

				$data_kecamatan = "73.24-LUWU TIMUR";

				break;

		}

		list($kode_kecamatan,$nama_kecamatan) = explode("-", $data_kecamatan);

		$session = array('kode_kecamatan' => $kode_kecamatan, 'nama_kecamatan' => $nama_kecamatan);

		$this->session->set_userdata($session);

		has_kode_kecamatan();

		// var_dump("masuk polling id_kecamatan ",$_SESSION['kode_kecamatan']);

		// die();

		$data['kecamatan']	= $_SESSION['nama_kecamatan'];

		// var_dump($_SESSION);

		$data['page'] 		= 'home';

		$data['title'] 		= 'Polling Kepuasan Pelanggan';

		$data['content'] 	= $this->load->view('polling/polling',$data,TRUE);

		$this->load->view('polling/layout',$data);

	}



	public function sub_domain() {

		// var_dump($_SERVER['HTTP_HOST']);

		// die();

		

		// redirect(base_url().'polling/home');

	}



	public function ajax($type=NULL) {

		$data = (object) array();

		switch ($type) {

			case 'get_kecamatan':

				$kecamatan['select']	= "*";

				$kecamatan['table']		= "m_kecamatan";

				$kecamatan['where']		= "NO_PROP = '73' AND NO_KAB = '24' ";



				$data->kecamatan 		= $this->m_polling->getAll($kecamatan);

				$return_kecamatan 		= "";

				foreach ($data->kecamatan as $k) {

					$return_kecamatan .= "<option value='".$k->NO_PROP.".".$k->NO_KAB.".".$k->NO_KEC."-".$k->NAMA_KEC."'>".$k->NAMA_KEC."</option>";

				}

				$data->kecamatan = $return_kecamatan;

				break;



			case 'set_kode_kecamatan':

				list($kode_kecamatan,$nama_kecamatan) = explode("-", $_POST['kode_kecamatan']);

				$session = array('kode_kecamatan' => $kode_kecamatan, 'nama_kecamatan' => $nama_kecamatan);

				$this->session->set_userdata($session);

				$data->redirect = base_url().'polling/home';

				$data->success = TRUE;

				break;



			case 'get_pertanyaan':

				$pertanyaan['select']	= "*";

				$pertanyaan['table']	= "m_pertanyaan";

				$pertanyaan['where']	= "status = 1 ";

				$pertanyaan['order']	= array('id_pertanyaan','ASC');

				$pertanyaan['limit']	= array($_GET['start'],'1');



				$data->pertanyaan 		= $this->m_polling->getAll($pertanyaan);

				if ($data->pertanyaan) {

					$data->status = TRUE;

				} else {

					$data->status = FALSE;

				}				

				break;



			case 'set_jawaban':

				$jawaban['select']	= "*";

				$jawaban['table']	= "m_jawaban";

				$jawaban['where']	= "status = 1 ";



				$data->jawaban 		= $this->m_polling->getAll($jawaban);

				$result_jawabaan 	= "";

				$icon = 1;

				foreach ($data->jawaban as $j) {

					switch ($icon) {

						case '1':

							$icon_src = base_url('assets/images/happy.png');

							break;

						case '2':

							$icon_src = base_url('assets/images/flat.png');

							break;

						

						default:

							$icon_src = base_url('assets/images/sad.png');

							break;

					}

					$result_jawabaan .= '<div class="col-md-4 col-sm-4 col-xs-4"><img class="img-responsive polling img-polling" id="'.$j->id_jawaban.'" src="'.$icon_src.'" title="'.$j->jawaban.'" onclick="next('.$j->id_jawaban.');" /><div class="jawaban-text">'.strtoupper($j->jawaban).'</div></div>';

					$icon++;

				}

				$data->jawaban 		= $result_jawabaan;		

				break;

			

			case 'save_polling':

				$polling['kode_kecamatan'] 		= $_SESSION['kode_kecamatan'];

				$polling['id_pertanyaan'] 		= $_POST['id_pertanyaan'];

				$polling['id_jawaban'] 			= $_POST['jawaban'];

				$polling['create_date'] 		= date('Y-m-d h:i:s');

				$polling['status'] 				= 1;



				// Insert t_polling

				$data_polling['data'] 	= $polling;

				$data_polling['table']	= "t_polling";

				$id_polling = $this->m_polling->addData($data_polling);

				if ($id_polling) {

					$data->status = TRUE;

				} else {

					$data->status = FALSE;

				}				

				break;

			default:

				echo "ALAMAT API YANG DIMASUKAN SALAH";

				exit();

				break;

		}

		echo json_encode($data);

	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */

