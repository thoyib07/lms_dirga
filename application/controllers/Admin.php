<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
	    parent::__construct();
	}

	public function date() {
		$date = date('Y-m-d H:i:s');
		return $date;
	}

	public function active_time() {
		$timer = 60 * 60; //60 menit
		if ($this->session->userdata('login') != 1) {
			redirect('admin/login');
		} elseif (time() - $this->session->userdata('time') > $timer) {
		 	$session = array('login' => FALSE, 'time' => '');
		 	$this->session->set_userdata($session);
		 	redirect('admin/index');
		}
 	}

	public function login() {
		$this->load->view('admin/login');
	}

	public function logout() {
		$this->session->unset_userdata('time');
 		$this->session->unset_userdata('login');
 		$this->session->unset_userdata('id_user');
 		$this->session->unset_userdata('akses');
 		$this->session->sess_destroy();
		redirect(base_url().'admin/');
	}

	public function relogin($id) {
		if ($this->session->userdata('id_user') == $id) {
			$session = array('login' => true, 'time' => time(), 'id_user' => $id);
			$this->session->set_userdata($session);
			//redirect(base_url().'admin/index');
		}
	}

	public function check() {
		$user = $_POST['user'];
		$pass = sha1($_POST['pass']);

		$login = array();

		$login['select'] 	= "*";
		$login['table']		= "m_user";
		$login['where']		= "user_name = '".$user."' AND password = '".$pass."' AND status = '1' ";
		$data['login'] 		= $this->m_admin->getAll($login);

		if ($data['login']) {
			$session = array('login' => true, 'time' => time(), 'id_user' => $data['login']['0']->id_user, 'akses' => $data['login']['0']->id_user_type);
			$this->session->set_userdata($session);
			redirect(base_url().'admin/index');
		} else {
			redirect(base_url().'admin/login');
		}
	}

	public function index() {
		session_start();
		$this->active_time();
		if ($this->session->userdata('login') == 1) {
			redirect(base_url().'admin/home');
		} else {
			redirect(base_url().'admin/login');
		}
	}

	public function home() {
		$this->active_time();
		$data['page'] 		= array('p' => 'home', 'c' => '' );
		$data['title'] 		= 'Admin Polling';

		$data['user'] 		= info_user();

		$smile['select']	= "id_polling";
		$smile['table']		= "t_polling";
		$smile['where'] 	= "status = 1 and id_jawaban = '1' ";
		$data['smile'] 		= $this->m_admin->count_getAll($smile);

		$flat['select']		= "id_polling";
		$flat['table']		= "t_polling";
		$flat['where'] 		= "status = 1 and id_jawaban = '2' ";
		$data['flat'] 		= $this->m_admin->count_getAll($flat);

		$sad['select']		= "id_polling";
		$sad['table']		= "t_polling";
		$sad['where'] 		= "status = 1 and id_jawaban = '3' ";
		$data['sad'] 		= $this->m_admin->count_getAll($sad);

		$data['content'] 	= $this->load->view('admin/home',$data,TRUE);
		$this->load->view('admin/layout',$data);
	}

	public function pertanyaan() {
		$this->active_time();
		$data['page'] 		= array('p' => 'pertanyaan', 'c' => '' );
		$data['title'] 		= 'Admin Polling';

		$data['user'] 		= info_user();
		$data['content'] 	= $this->load->view('admin/pertanyaan',$data,TRUE);
		$this->load->view('admin/layout',$data);
	}

	public function user() {
		$this->active_time();
		$data['page'] 		= array('p' => 'home', 'c' => '' );
		$data['title'] 		= 'Admin Polling';

		$data['user'] 		= info_user();
		$data['content'] 	= $this->load->view('admin/home',$data,TRUE);
		$this->load->view('admin/layout',$data);
	}

	public function ajax_get_data($id) {
		$data = array();
		switch ($_GET['type']) {
			case 'pertanyaan':
				$pertanyaan['select']	= "*";
				$pertanyaan['table']	= "m_pertanyaan";
				$pertanyaan['where']	= "status = '1' AND id_pertanyaan = '".$id."'";
				$data['detail']			= $this->m_admin->getAll($pertanyaan);
				break;
			
			default:
				# code...
				break;
		}
		echo json_encode($data);
	}

	public function ajax_insert() {
		$data = array();		
		$create_date = date('Y-m-d H:i:s');
		switch ($_POST['type']) {
			case 'pertanyaan':
				$this->validate_pertanyaan();
				$pertanyaan['pertanyaan'] 	= $_POST['pertanyaan'];
				$pertanyaan['create_date'] 	= $create_date;
				$pertanyaan['status'] 		= 1;

				// Insert m_pertanyaan
				$data_pertanyaan['data'] 	= $pertanyaan;
				$data_pertanyaan['table']	= "m_pertanyaan";
				$id_pertanyaan = $this->m_admin->addData($data_pertanyaan);
				$data['status'] = TRUE;
				break;
			
			default:
				# code...
				break;
		}
        echo json_encode($data);
	}

	public function ajax_update($id) {
		$data = array();		
		$update_date = date('Y-m-d H:i:s');
		switch ($_POST['type']) {
			case 'pertanyaan':
				$this->validate_pertanyaan();
				$pertanyaan['pertanyaan'] 	= $_POST['pertanyaan'];
				$pertanyaan['update_date'] 	= $update_date;
				$pertanyaan['status'] 		= 1;

				// Insert m_pertanyaan
				$data_pertanyaan['data'] 		= $pertanyaan;
				$data_pertanyaan['table']		= "m_pertanyaan";
				$data_pertanyaan['where'][0]	= array('id_pertanyaan', $id);
				$id_pertanyaan = $this->m_admin->updateData($data_pertanyaan);
				$data['status'] = TRUE;
				break;
			
			default:
				# code...
				break;
		}
        echo json_encode($data);
	}

	public function ajax_del($id) {
		$data['id']		= $id;
		$data['field']	= $_GET['field'];
		$data['table']	= $_GET['table'];

		$this->m_admin->delById($data);
		echo json_encode($data);
	}

	public function ajax_restore($id) {
		$data['id']		= $id;
		$data['field']	= $_GET['field'];
		$data['table']	= $_GET['table'];

		$this->m_admin->restorById($data);
		echo json_encode($data);
	}

	public function ajax_create_report() {

		$kop_kota = "PEMERINTAH KABUPATEN LUWU TIMUR";
		$kop_dinas = "DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL";
		$kop_alamat = "JL. SOEKARNO HATTA, PUNCAK INDAH";
		$kop_alamat_kota = "MALILI, 92981";

		$data = (object) array();
		$kecamatan['select']	= "*";
		$kecamatan['table']		= "m_kecamatan";
		$kecamatan['where']		= "NO_PROP = '73' AND NO_KAB = '24' ";

		$data_kecamatan 		= $this->m_polling->getAll($kecamatan);
		$sheet = array();
		$kode_wilayah = array();
		
		array_push($sheet, 'TOTAL');
		array_push($kode_wilayah, '');
		
		array_push($sheet, 'LUWU TIMUR');
		array_push($kode_wilayah, '73.24');
		foreach ($data_kecamatan as $k) {
			array_push($sheet, $k->NAMA_KEC);
			array_push($kode_wilayah, $k->NO_PROP.".".$k->NO_KAB.".".$k->NO_KEC);
		}
		$data->sheet = $sheet;
		$data->kode_wilayah = $kode_wilayah;
		// echo json_encode($data);
		// die();
		if ( (isset($_POST['start_date'])) AND ($_POST['start_date'] !== "") ) {
			$start_date = $_POST['start_date'];
		} else {
			$start_date = NULL;
		}

		if ( (isset($_POST['end_date'])) AND ($_POST['end_date'] !== "") ) {
			$end_date = $_POST['end_date'];
		} else {
			$end_date = NULL;
		}
		// $data = base_url('admin/ajax_report?').'kode_kecamatan='.$kode_kecamatan.'&start_date='.$start_date.'&end_date='.$end_date.'&type='.$type_report;
		// echo json_encode($data);
		$objPHPExcel = new PHPExcel();
		// $sheet = $objPHPExcel->getActiveSheet();

	    //Start adding next sheets
	    $i=0;
	    foreach ($data->sheet as $s) {
	      	// Add new sheet
	      	$objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating

	      	$report['select']	= "t.pertanyaan, j.jawaban, COUNT(p.kode_kecamatan) AS total_jawaban, p.kode_kecamatan,p.id_pertanyaan, p.id_jawaban";
			$report['table']	= "t_polling AS p";
			$report['join'][0]	= array('m_pertanyaan AS t', 'p.id_pertanyaan = t.id_pertanyaan');
			$report['join'][1]	= array('m_jawaban AS j', 'p.id_jawaban = j.id_jawaban');
			$report['where']	= "p.status = '1' ";
			$report['where']	.= "AND p.kode_kecamatan = '".$data->kode_wilayah[$i]."' ";

			if ($data->kode_wilayah[$i]=="73.24" && $data->sheet[$i]=="LUWU TIMUR") {
				$objWorkSheet->setCellValue('A6', 'Kantor Dinas');
	      	}
			else if ($data->kode_wilayah[$i]=="" && $data->sheet[$i]=="TOTAL") {
				$objWorkSheet->setCellValue('A6', 'Kabupaten');
			}
			else {
				$objWorkSheet->setCellValue('A6', 'Kecamatan '.$data->sheet[$i]);
	      	}
			//Write cells
	      	if (($start_date == NULL) AND ($end_date == NULL)) {
	      		$objWorkSheet->setCellValue('A5', 'Periode :  => ');
	      	} elseif ($start_date == NULL) {
	      		$objWorkSheet->setCellValue('A5', 'Periode : => '.date('d-m-Y',strtotime($end_date)));

	      		$report['where']	.= "AND p.create_date <= '".date('Y-m-d',strtotime($end_date))."' ";
	      	} elseif ($end_date == NULL) {
	      		$objWorkSheet->setCellValue('A5', 'Periode : '.date('d-m-Y',strtotime($start_date)).' => ');

	      		$report['where']	.= "AND p.create_date >= '".date('Y-m-d',strtotime($start_date))."' ";
	      	} else {
	      		$objWorkSheet->setCellValue('A5', 'Periode : '.date('d-m-Y',strtotime($start_date)).' => '.date('d-m-Y',strtotime($end_date)));

	      		$report['where']	.= "AND p.create_date <= '".date('Y-m-d',strtotime($end_date))."' ";
	      		$report['where']	.= "AND p.create_date >= '".date('Y-m-d',strtotime($start_date))."' ";
	      	}

			$report['group']	= array('p.kode_kecamatan,p.id_pertanyaan, p.id_jawaban');
			$data->report		= $this->m_admin->getAll($report);      	
			// echo json_encode($data->report);
			// die();
            // $objWorkSheet->setCellValue('A12', 'Pertanyaan');
            // $objWorkSheet->setCellValue('B12', 'Jawaban');
            // $objWorkSheet->setCellValue('C12', 'Total Jawaban');

			$data_jawaban = $this->m_admin->getAllJawaban();
			
			$data_pertanyaan = $this->m_admin->getAllPertanyaan();
			$column_j = 'A';
			$column_p = 'A';
			foreach($data_pertanyaan as $r) {
				for ($x=0; $x<count($data_jawaban); $x++) {
					$cell_j = $column_j. '9';
					$objPHPExcel->getActiveSheet()->getColumnDimension($column_j)->setWidth(15);
					$objWorkSheet->setCellValue($cell_j, strtoupper($data_jawaban[$x]['jawaban']));
					$objWorkSheet->getStyle($cell_j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objWorkSheet->getStyle($cell_j)->getFont()->setBold(true);
					$objWorkSheet->getStyle($cell_j)->getFont()->setSize(10);
					
					$cell_pol = $column_j.'10';
					$data_polling = $this->m_admin->getPolling($data->kode_wilayah[$i], $r['id_pertanyaan'], $data_jawaban[$x]['id_jawaban']);
					$objWorkSheet->setCellValue($cell_pol, count($data_polling));
					$objWorkSheet->getStyle($cell_pol)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objWorkSheet->getStyle($cell_pol)->getFont()->setSize(11);
					
					if (count($data_jawaban)-($x+1)>0)
						$column_j++;
				}
				$rangeCell = $column_p .'8:'.($column_j--).'8';
				$objWorkSheet->mergeCells($rangeCell);
				$objWorkSheet->setCellValue($column_p. '8', strtoupper($r['pertanyaan']));
				$objWorkSheet->getStyle($rangeCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objWorkSheet->getStyle($rangeCell)->getFont()->setBold(true);
				$objWorkSheet->getStyle($rangeCell)->getFont()->setSize(12);

				$column_j++;
				$objPHPExcel->getActiveSheet()->getColumnDimension($column_j)->setWidth(2);
				$column_j++;
				$column_p = $column_j;
				
			}

	      	// Rename sheet
	      	$objWorkSheet->setTitle($s);

	  // 		$objWorkSheet->getStyle('A1')->applyFromArray(
			//     array(
			//         'fill' => array(
			//             'type' => PHPExcel_Style_Fill::FILL_SOLID,
			//             'color' => array('rgb' => 'FF0000')
			//         )
			//     )
			// );
			$objWorkSheet->mergeCells('C1:O1');
			$objWorkSheet->setCellValue('C1', $kop_kota);

			$objWorkSheet->mergeCells('C2:O2');
			$objWorkSheet->setCellValue('C2', $kop_dinas);

			$objWorkSheet->mergeCells('C3:O3');
			$objWorkSheet->setCellValue('C3', $kop_alamat);

			$objWorkSheet->mergeCells('C4:O4');
			$objWorkSheet->setCellValue('C4', $kop_alamat_kota);
			$objWorkSheet->getStyle('A1:O4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    		$objWorkSheet->getStyle('A1:O2')->getFont()->setBold(true);
    		$objWorkSheet->getStyle('A1:O2')->getFont()->setSize(14);
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('logo');
			$objDrawing->setDescription('logo');
			$objDrawing->setPath('assets/images/luwu_timur_logo.png');
			$objDrawing->setCoordinates('A1');
			$objDrawing->setOffsetX(50); 
			$objDrawing->setOffsetY(5);       
			$objDrawing->setWidth(200); 
			$objDrawing->setHeight(80); 
			$objDrawing->setWorksheet($objWorkSheet);
			$i++;
	    }

		// echo date('H:i:s') , " Write to Excel2007 format" , EOL;
		$callStartTime = microtime(true);
		$target = 'assets/report/';
		$name_file = strtoupper(REPORT_NAME).'_'.date('Y-m-d').'.xlsx';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

		// echo date('H:i:s') , " Moving file to another folder" , EOL;
		rename(str_replace('.php', '.xlsx', __FILE__), $target.$name_file);
		echo json_encode(base_url('assets/report/').$name_file);
	}

	public function ajax_report() {
		var_dump("<hr>",$_GET);
		if ($_GET['type'] == "Kecamatan") {
			$data['start_date'] = "";
			$data['end_date'] = "";
			$report['select']	= "k.NAMA_KEC AS nama, t.pertanyaan, j.jawaban, COUNT(p.kode_kecamatan) AS total_jawaban, p.kode_kecamatan,p.id_pertanyaan, p.id_jawaban";
			$report['table']	= "t_polling AS p";
			$report['join'][0]	= array('m_pertanyaan AS t', 'p.id_pertanyaan = t.id_pertanyaan');
			$report['join'][1]	= array('m_jawaban AS j', 'p.id_jawaban = j.id_jawaban');
			$report['join'][2]	= array('m_kecamatan AS k', 'p.kode_kecamatan = CONCAT(NO_PROP,".",NO_KAB,".",NO_KEC)');
			$report['where']	= "p.status = '1' ";

			if ( (isset($_GET['kode_kecamatan'])) AND ($_GET['kode_kecamatan'] !== "") ) {
				$report['where']	.= "AND p.kode_kecamatan = '".$_GET['kode_kecamatan']."' ";
			}
			if ( (isset($_GET['start_date'])) AND ($_GET['start_date'] !== "") ) {
				$data['start_date'] = date('Y-m-d',strtotime($_GET['start_date']));
				$report['where']	.= "AND p.create_date >= '".$data['start_date']."' ";
			}
			if ( (isset($_GET['end_date'])) AND ($_GET['end_date'] !== "") ) {
				$data['end_date'] = date('Y-m-d',strtotime($_GET['end_date']));
				$report['where']	.= "AND p.create_date <= '".$data['end_date']."' ";
			}

			$report['group']	= array('p.kode_kecamatan,p.id_pertanyaan, p.id_jawaban');
			$data['report']		= $this->m_admin->getAll($report);

			$this->load->view('admin/report_polling',$data);
		} else {
			$data['start_date'] = "";
			$data['end_date'] = "";
			$report['select']	= "k.NAMA_KAB AS nama, t.pertanyaan, j.jawaban, COUNT(p.kode_kecamatan) AS total_jawaban, p.kode_kecamatan,p.id_pertanyaan, p.id_jawaban";
			$report['table']	= "t_polling AS p";
			$report['join'][0]	= array('m_pertanyaan AS t', 'p.id_pertanyaan = t.id_pertanyaan');
			$report['join'][1]	= array('m_jawaban AS j', 'p.id_jawaban = j.id_jawaban');
			$report['join'][2]	= array('m_kabupaten_kota AS k', 'p.kode_kecamatan = CONCAT(NO_PROP,".",NO_KAB)');
			$report['where']	= "p.status = '1' ";

			if ( (isset($_GET['kode_kecamatan'])) AND ($_GET['kode_kecamatan'] !== "") ) {
				$report['where']	.= "AND p.kode_kecamatan = '".$_GET['kode_kecamatan']."' ";
			}
			if ( (isset($_GET['start_date'])) AND ($_GET['start_date'] !== "") ) {
				$data['start_date'] = date('Y-m-d',strtotime($_GET['start_date']));
				$report['where']	.= "AND p.create_date >= '".$data['start_date']."' ";
			}
			if ( (isset($_GET['end_date'])) AND ($_GET['end_date'] !== "") ) {
				$data['end_date'] = date('Y-m-d',strtotime($_GET['end_date']));
				$report['where']	.= "AND p.create_date <= '".$data['end_date']."' ";
			}

			$report['group']	= array('p.kode_kecamatan,p.id_pertanyaan, p.id_jawaban');
			$data['report']		= $this->m_admin->getAll($report);

			$this->load->view('admin/report_polling',$data);
		}
	}

	public function ajax_list() {
		$data 	= array();
        $no 	= $_POST['start'];
        $type 	= $_GET['type'];
        if (isset($_GET['id'])) {
        	$id 	= $_GET['id'];
        }
        switch ($type) {
        	case 'user':
        	case 'del_user':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = $l->nama;
		            $row[] = $l->SKPD;
		            $row[] = $l->jabatan;
		            

		            if ($type == 'user') {
		            	$row[] = '<button onclick="ubah('.$l->id_user.')" class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data User"></button>'.''.'<button onclick="hapus('.$l->id_user.')" class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data User"></button>';
		            } else {
		            	 $row[] = '<button onclick="restore('.$l->id_user.')" class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data User"></button>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        	break;

        	case 'list_pertanyaan':
        	case 'list_del_pertanyaan':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = $l->pertanyaan;		            

		            if ($type == 'list_pertanyaan') {
		            	$row[] = '<button onclick="edit('.$l->id_pertanyaan.')" class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data Pertanyaan"></button>'.''.'<button onclick="del('.$l->id_pertanyaan.')" class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data Pertanyaan"></button>';
		            } else {
		            	 $row[] = '<button onclick="restore('.$l->id_pertanyaan.')" class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data Pertanyaan"></button>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        	break;
        	
        	default:
        		# code...
        		break;
        }
        //output to json format
        echo json_encode($output);
	}

	private function validate_pertanyaan() {
        $data = array();
        $data['error_class'] = array();
        $data['error_string'] = array();
        $data['status'] = TRUE;

        if($_POST['pertanyaan'] == ''){
            $data['error_class']['pertanyaan'] = 'has-error';
            $data['error_string']['pertanyaan'] = 'Pertanyaan tidak boleh kosong';
            $data['status'] = FALSE;
        }

        if($data['status'] == FALSE){
            echo json_encode($data);
            exit();
        }
        // echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
