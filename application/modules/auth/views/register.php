
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $title; ?> | Register Page</title>

    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/admin/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="loginColumns animated fadeInDown">
		<div class="row">

			<div class="col-md-4">
				<h2 class="font-bold">Selamat Datang</h2>
				<br/>
				<p>
					BAPPEDA KOTA TANGERANG
				</p>
			</div>
			<div class="col-md-8">
				<div class="ibox-content">
				<script type="text/javascript">
					function validation() {
						var nama 	= document.forms["form"]["nama"].value;
						var b_date 	= document.forms["form"]["b_date"].value;
						var hp 		= document.forms["form"]["hp"].value;
						var alamat 	= document.forms["form"]["alamat"].value;
						var email 	= document.forms["form"]["email"].value;
						var user 	= document.forms["form"]["user"].value;
						var pass 	= document.forms["form"]["pass"].value;

					    if (nama == "") {
					        document.getElementById("nama").style.borderColor = "#E34234"; alert("Nama Harus Diisi");
					        return false;
					    }

					    if (b_date == "") {
					        document.getElementById("b_date").style.borderColor = "#E34234"; alert("Tanggal Lahir Harus Diisi");
					        return false;
					    }

					    if (hp == "") {
					        document.getElementById("hp").style.borderColor = "#E34234"; alert("No Handphone Harus Diisi");
					        return false;
					    }

					    if (alamat == "") {
					        document.getElementById("alamat").style.borderColor = "#E34234"; alert("Alamat Harus Diisi");
					        return false;
					    }

					    if (email == "") {
					        document.getElementById("email").style.borderColor = "#E34234"; alert("Email Harus Diisi");
					        return false;
					    }

					    if (user == "") {
					        document.getElementById("user").style.borderColor = "#E34234"; alert("Username Harus Diisi");
					        return false;
					    }

					    if (pass == "") {
					        document.getElementById("pass").style.borderColor = "#E34234"; alert("Password Harus Diisi");
					        return false;
					    }
					}
				</script>
					<form id="form" class="m-t" role="form" action="<?php echo $rand;  ?>" onsubmit="return validation()" id="login-form" method="post">
						<div class="form-group">
							<h3>Register</h3>
						</div>
						<div class="form-group">
							<!-- Nama -->
							<input id="nama" type="text" name="nama" class="form-control" placeholder="Nama Lengkap *" >
						</div>
						<div class="form-group">
							<!-- Kelamin -->
							<select class="form-control" name="sex" >
								<option value="1">Laki-Laki</option>
								<option value="0">Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<!-- Tanggal Lahir -->
							<input id="b_date" type="date" name="tanggal" class="form-control" placeholder="Tanggal Lahir *" >
						</div>
						<div class="form-group">
							<!-- Telp -->
							<input id="telp" type="text" name="telp" class="form-control" pattern="[0-9]{0,13}" maxlength="13" placeholder="Telepon">
						</div>
						<div class="form-group">
							<!-- HP -->
							<input id="hp" type="text" name="hp" class="form-control" pattern="[0-9]{0,13}" maxlength="13" placeholder="No Handphone *" >
						</div>
						<div class="form-group">
							<!-- Alamat -->
							<textarea id="alamat" name="alamat" class="form-control" placeholder="Alamat *" ></textarea>
						</div>
						<div class="form-group">
							<!-- Kode Post -->
							<input id="zip" type="text" name="zip" class="form-control" pattern="[0-9]{0,5}" maxlength="5" placeholder="Kode Post">
						</div>
						<div class="form-group">
							<!-- Email -->
							<input id="email" type="email" name="email" class="form-control" placeholder="Email *" >
						</div>
						<div class="form-group">
							<!-- Username -->
							<input id="user" type="text" name="user" class="form-control" placeholder="Username *" >
						</div>
						<div class="form-group">
							<!-- Password -->
							<input id="pass" type="password" name="pass" class="form-control" placeholder="Password *" >
						</div>
						<div class="form-group">
							<!-- Confirm Password -->
							<input id="pass2" type="password" name="conf_pass" class="form-control" placeholder="Ulangi Password *" >
						</div>
						<div class="form-group">
							<span>* Wajib Diisi</span>
						</div>
						<input type="submit" name="submit" value="DAFTAR" class="btn btn-primary block full-width m-b">
						
					</form>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row">
			<!--<div class="col-md-6">
				Delivered to you by <strong style="text-transform:uppercase;">TAD</strong>
			</div>
			<div class="col-md-6 text-right">
			   <small>© 2017</small>
			</div>-->
		</div>
	</div>
	
	<script src="<?php echo base_url();?>assets/admin/js/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#pass2").change(checkPasswordMatch);

			function checkPasswordMatch() {
			    var password = $("#pass").val();
			    var confirmPassword = $("#pass2").val();

			    if (password != confirmPassword) {
			    	document.getElementById("pass").style.borderColor = "#E34234";
        			document.getElementById("pass2").style.borderColor = "#E34234";
			        alert("Passwords Tidak Sama");
			    }
			}
		});
	</script>
</body>

</html>