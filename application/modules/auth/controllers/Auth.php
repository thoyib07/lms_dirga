<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index() {
		// echo "auth/index";
		// $this->login();
		redirect(base_url().'admin/');
	}

	public function login() {
		//var_dump('masuk');
		$data['title'] = "LMS Dirga";
		$this->load->view('login',$data);
	}

	public function logout() {
		$this->session->unset_userdata('cms_time');
 		$this->session->unset_userdata('cms_login');
		$this->session->unset_userdata('cms_id_user');
 		$this->session->unset_userdata('cms_akses');
 		$this->session->sess_destroy();
		redirect(base_url().'admin/');
	}

	public function check() {
		$user 		= $_POST['user'];
	  	$pass		= sha1($_POST['pass']);

	  	$data_user['select'] 	= "*";
  		$data_user['table']		= "m_user";
  		$data_user['where']		= "username = '".$user."' and password = '".$pass."' and status = '1'";

  		$data['user'] = $this->m_admin->getAll($data_user);
  		$session = array('cms_login' => true, 'cms_time' => time(), 'cms_id_user' => $data['user']['0']->id_user,'cms_akses' => $data['user']['0']->id_user_level);

		$this->session->set_userdata($session);
		redirect(base_url().'admin/index');
  			
	}

	public function forget($token) {		
		if (isset($token)) {
			//cek user yang punya token
			//set password baru
			//set ulang random di table user
		} else {
			if (isset($_POST)) {
			$data['select']	= 'email';
			$data['table']	= 'user';
			$data['where']	= "username = '".$_POST['user']."' and email LIKE '%".$_POST['email']."%'";
			$data['limit']	= "limit 1";
			$email = $this->m_admin->getLimit($data);
			if ($_POST['email'] == $email[0]->email) {
				$value = array('email' => $_POST['email'], 'user' => $_POST['user'], 'token' => random(), 'date' => date('Y-m-d H:i:s'));
				email($value);
			}
			var_dump($_POST);die();
			} else {
				redirect(base_url().'auth/login');
			}
		}
	}

	public function relogin($id) {
		if ($this->session->userdata('id_user') == $id) {
			$session = array('login' => true, 'time' => time(), 'id_user' => $id);
			$this->session->set_userdata($session);
			//redirect(base_url().'admin/index');
		}
	}

	// public function register($rand) {
	// 	$data['select']	= 'rand';
	// 	$data['table']	= 'random';
	// 	$data['where']	= "type = 1";
	// 	$data['rand'] = $this->m_admin->getAll($data);
	// 	if ($rand == $data['rand'][0]->rand) {
	// 		if (isset($_POST['submit'])) {
	// 			$data['name'] 			= $_POST['nama'];
	// 			$data['sex'] 			= $_POST['sex'];
	// 			$data['birthday'] 		= $_POST['tanggal'];
	// 			$data['phone'] 			= $_POST['telp'];
	// 			$data['mobile'] 		= $_POST['hp'];
	// 			$data['address'] 		= $_POST['alamat'];
	// 			$data['zip'] 			= $_POST['zip'];
	// 			$data['email'] 			= $_POST['email'];
	// 			$data['username'] 		= $_POST['user'];
	// 			$data['password'] 		= md5(crc32($_POST['pass'].'J8ih6y'));
	// 			$data['activation_key'] = random();

	// 			$this->m_admin->addUser($data);
	// 			redirect(base_url().'admin/');
	// 		} else {
	// 			$data['title'] = "SISTAD";
	// 			$data['zone'] = "tad";
	// 			$data['rand'] = $rand;
	// 			$this->load->view('register',$data);
	// 		}
	// 	} else {
	// 		redirect(base_url());
	// 	}		
	// }
}
