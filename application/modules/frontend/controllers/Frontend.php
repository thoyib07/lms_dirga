<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {
	public function index() {
		// $data['content'] = $this->load->view('home',$data,TRUE);
		// $this->load->view('welcome_message');

		// $this->load->view('parts/base_view');
		$data['menu'] = "";
		$data['user'] = "";

		$peristiwa['select'] 	= "id_era,peristiwa,nilai_min,nilai_max";
		$peristiwa['from'] 		= "m_era";
		$peristiwa['where'] 	= "status = 1";
		$data['peristiwa']	 	= $this->m_frontend->getAll($peristiwa);
		$data['c_peristiwa'] 	= $this->m_frontend->c_getAll($peristiwa);

		$soal['select'] = "id_era_soal,pernyataan,pilihan_min,pilihan_max,reverse";
		$soal['from'] 	= "m_era_soal";
		$soal['where'] 	= "status = 1";
		$data['soal']	= $this->m_frontend->getAll($soal);

		$data['tampilan_menu'] = $this->load->view('parts/menu',$data,true);
		$data['content'] = $this->load->view('home_lms','',true);
		$data['judul'] = 'Home';

		$this->load->view('parts/base_view',$data);
	}

	public function simpan(){
		$peristiwa['select'] 	= "id_era,peristiwa,nilai_min,nilai_max";
		$peristiwa['from'] 		= "m_era";
		$peristiwa['where'] 	= "status = 1";
		$data['peristiwa'] 		= $this->m_frontend->getAll($peristiwa);
		$data['c_peristiwa'] 	= $this->m_frontend->c_getAll($peristiwa);

		$soal['select'] 	= "id_era_soal,pernyataan,pilihan_min,pilihan_max,reverse";
		$soal['from'] 		= "m_era_soal";
		$soal['where'] 	= "status = 1";
		$data['soal'] 	= $this->m_frontend->getAll($soal);
		$data['c_soal'] 	= $this->m_frontend->c_getAll($soal);

		$counter = 1;

		$data_array_peristiwa = array();
		$data_array_soal = array();
		$cont_peristiwa = 0;
		for ($i=1; $i <= $data['c_peristiwa']; $i++) { 
			$cont_soal = 0;
			for ($j=1; $j <= $data['c_soal']; $j++) { 	
				$label = "per".$i."_soal".$j;
				if (isset($_POST[$label])) {
					$data_post = $_POST[$label];
				}else{
					$data_post = 0;
				}
				$peserta_jawab['data'] = array('id_peserta' => "1", 'id_era' => $data['peristiwa'][$i-1]->id_era , 'id_era_soal' => $data['soal'][$j-1]->id_era_soal, 'jawaban' => $data_post);
				$peserta_jawab['from'] = "t_peserta_jawaban_era";
				$this->m_frontend->addData($peserta_jawab);

				$cont_soal = $cont_soal + $data_post;
			}

			if ($counter == 2) {
				$cont_peristiwa = $cont_peristiwa + $cont_soal;
				$cont_peristiwa = $cont_peristiwa / 10;
				$hasil_soal['hasil_nilai_'.$i] = $cont_peristiwa;
				$counter = 0;
				$cont_peristiwa = 0;
			}else{
				$cont_peristiwa = $cont_peristiwa + $cont_soal;
			}
			$counter++;

		}
		$message = "Berhasil";
		echo json_encode($hasil_soal);
	}

	public function get_data($value) {
		# code...
	}
}
