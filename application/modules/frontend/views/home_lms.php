<?php 
  $c_kel = ceil($c_peristiwa/2); 
  $counter_kel = 1; 
  $counter_peristiwa = 1;
  $no_peristiwa = 1; 
  $no_soal = 1; 
?>
<script  type="text/javascript">
  var hal = 1; 
  $(document).ready(function(){
      // console.log(hal);
      $('#soal_bagian_'+hal).show();
      $('#btn_selanjutnya').show();

  });

  function next(){
      hal = hal + 1;
      // console.log(hal);
      $('#soal_bagian_'+(hal-1)).hide();
      $('#soal_bagian_'+hal).show();
      $('#btn_kebali').show();
      if (hal == <?php echo $c_kel; ?>) {
        $('#btn_simpan').show();     
        $('#btn_selanjutnya').hide();
      } else {  
        $('#btn_selanjutnya').show();
      }
  }

  function prev(){
      hal = hal - 1;
      // console.log(hal);
      $('#soal_bagian_'+(hal+1)).hide();
      $('#soal_bagian_'+hal).show();
      $('#btn_selanjutnya').show();
      $('#btn_simpan').hide();  
      if (hal == 1) {   
        $('#btn_kebali').hide();
      } else {  
        $('#btn_kebali').show();        
      }
  }

  function simpan(){
    var formData = $('#form').serialize();
    $.ajax({
        url : "<?php echo site_url('Frontend/simpan')?>",
        type: "POST",
        data: formData,
        dataType: "JSON",
        success: function(response) {
            // swal("Data Berhasil Di simpan!", "Hasil Tes ERA \n Resiko Bisnis : "+response.hasil_nilai_2+"\n Resiko Jabatan : "+response.hasil_nilai_4+"\n Resiko Individu : "+response.hasil_nilai_6, "success");
            // window.open("<?php // echo base_url(); ?>");
            swal({
              title: "Data Berhasil Di simpan!",
              text: "Hasil Tes ERA \n Resiko Bisnis : "+response.hasil_nilai_2+"\n Resiko Jabatan : "+response.hasil_nilai_4+"\n Resiko Individu : "+response.hasil_nilai_6,
              type: "success",
              confirmButtonText: "Menuju Test Berikutnya!"
            },
            function(){
              window.location.href = "<?php echo base_url(); ?>";
            });
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Terjadi Kesalahan!", "Anda Sudah Pernah Mengikuti Ujian Ini!", "error");
 
        }
        
    });
  }
</script>


<div class="content">
  <form action="#" id="form" class="form-horizontal form-label-left" novalidate>
    <?php 
      foreach ($peristiwa as $p) {  //$counter_soal = 1;
        // cek awal dari kelompok penilaian
        if ($counter_peristiwa == 1) { 
          echo '<div id="soal_bagian_'.$counter_kel.'" style="display: none;">'; 
        }

        echo '<div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Soal no '.$no_peristiwa.'</h3>
          </div>

          <div class="box-body">
            <textarea class="form-control" rows="3" placeholder="" disabled>'.$p->peristiwa.'</textarea>
            <br>';

        foreach ($soal as $s) {
          echo $s->pernyataan.'
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="'.$s->pilihan_min.'" disabled>
            </div>';

            // Nilai Terbalik
            if ($s->reverse == 0) {
              echo '<div class="col-xs-4" style="text-align: center;">
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" value="1">1</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="2">2</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="3">3</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="4">4</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="5">5</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="6">6</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="7">7</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="8">8</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="9">9</lable>
              </div>';
            } else {
              echo '<div class="col-xs-4" style="text-align: center;">
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" value="9">1</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="8">2</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="7">3</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="6">4</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="5">5</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="4">6</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="3">7</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="2">8</lable>
                <lable><input type="radio" name="per'.$no_peristiwa.'_soal'.$no_soal.'" style="margin-left: 5px;" value="1">9</lable>
              </div>';
            }

            echo'<div class="col-xs-4">
              <input type="text" class="form-control" placeholder="'.$s->pilihan_max.'" disabled>
            </div>
          </div><hr>';

          // Cek apakah soal terakhir
          if ($no_soal == 5) {
            $no_soal = 0;
          }
          $no_soal++;
        }

        echo '</div>
        </div>';

        // cek apakah akhir dari kelompok penilaian
        if ($counter_peristiwa == 2) {
          $counter_peristiwa = 0;
          $counter_kel++;
          echo "</div>";
        }

        $no_peristiwa++;
        $counter_peristiwa++;
    ?>
    <?php } ?>
    
    <!-- <div id="soal_bagian_1" style="display: none;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 1</h3>
        </div>

        <div class="box-body">
          <textarea class="form-control" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>

          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil1" value="1">1 
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil2" value="9">1 
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="1">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil3" value="1">1 
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil4" value="1">1 
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil5" value="1">1 
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 2</h3>
        </div>

        <div class="box-body">
          <textarea class="form-control" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil1" value="9">1 
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil2" value="9">1 
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil3" value="9">1 
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil4" value="9">1 
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil5" value="9">1 
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>
    </div> -->

  </form> 

  <div class="box-footer">
    <button type="submit" class="btn btn-danger" id="btn_kebali" onclick="prev()" style="display: none;">Kembali</button>
    <button type="submit" class="btn btn-warning" id="btn_selanjutnya" onclick="next()" style="display: none;">Soal selanjutnya</button>
    <button type="submit" class="btn btn-success" id="btn_simpan" onclick="simpan()" style="display: none;">simpan</button>
  </div>
</div>