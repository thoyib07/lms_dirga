
<!DOCTYPE html>
<html>
<?php include 'part/head.php'; ?>
<body class="hold-transition skin-blue sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">

  <?php include 'part/header.php'; ?>

  <?php include 'part/side_menu.php'; ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div id="content" class="content-wrapper">
    <?php if (isset($content)) {
      echo $content;
    } else {
      echo "SISTAD";
    } ?>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <!--
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
    -->
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/'; ?>js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/admin/'; ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/admin/'; ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<!-- <script src="<?php // echo base_url().'assets/admin/'; ?>dist/js/app.min.js"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php // echo base_url().'assets/admin/'; ?>dist/js/demo.js"></script> -->

<script type="text/javascript">
  $(document).ready(function(){
    // var timeStamp = Math.floor(Date.now() / 1000);
    // var sisa = timeStamp - <?php echo $this->session->userdata('time'); ?>;
    // console.log(sisa);
    // setInterval(function() {
    // // cek time duration login 60 menit
    // //console.log('cek login ',sisa);
    // switch (true) {
    //   case (sisa >= 3000):
    //     if (confirm('sisa waktu anda kurang 10 menit, apakah ingin diperpanjang?')) {
    //       $.ajax({
    //         url: '<?php echo base_url()."auth/relogin/".$this->session->userdata('id_user'); ?>',
    //         type: "POST",
    //         data: {
    //             //console.log('masuk sini bray');
    //         },
    //         success: function () {
    //             window.location.replace("<?php echo current_url(); ?>");
    //         }
    //       });
    //     }
    //     break;
    //   default:
    // }
    // }, 60 * 1000); // 60 * 1000 milsec
  });
  /*
  $('#link').click(function() {
    var link = document.getElementById("link").getAttribute("data-link");
     $.ajax({
            url: link,
            type: "POST",
            data: {
                // data stuff here
            },
            success: function () {
                // does some stuff here...
            }
          });
  });
  */
  function content(url,type) {
    $('#content').html("");
      $.ajax({
          type:'post',
          url: url,// put your real file name 
          // data:{id: id},
          success:function(msg){
            switch (type) {
              case 'home':
                $('.menu').removeClass("active");
                $('#menu_user').addClass("active");
              break;

              case 'ktda':
                $('.menu').removeClass("active");
                $('#menu_ktda').addClass("active");
              break;

              case 'sub_ktda':
                $('.menu').removeClass("active");
                $('#menu_sub_ktda').addClass("active");
              break;

              case 'upload':
                $('.menu').removeClass("active");
                $('#menu_upload').addClass("active");
              break;
              
              case 'user':
                $('.menu').removeClass("active");
                $('#menu_user').addClass("active");
              break;
            }            
            $('#content').append(msg);        
          }
      });
  }
</script>
</body>
</html>
