
<script  type="text/javascript">
  var hal = 1; 
  $(document).ready(function(){
      console.log(hal);
      $('#soal_bagian_'+hal).show();
      $('#btn_selanjutnya').show();

  });

  function next(){
      hal = hal + 1;
      console.log(hal);
      $('#soal_bagian_'+(hal-1)).hide();
      $('#soal_bagian_'+hal).show();
      $('#btn_kebali').show();
      if (hal == 3) {
        $('#btn_simpan').show();     
        $('#btn_selanjutnya').hide();
      } else {  
        $('#btn_selanjutnya').show();
      }
  }

  function prev(){
      hal = hal - 1;
      console.log(hal);
      $('#soal_bagian_'+(hal+1)).hide();
      $('#soal_bagian_'+hal).show();
      $('#btn_selanjutnya').show();
      $('#btn_simpan').hide();  
      if (hal == 1) {   
        $('#btn_kebali').hide();
      } else {  
        $('#btn_kebali').show();        
      }
  }

  function simpan(){
    var formData = $('#form').serialize();
    $.ajax({
        url : "<?php echo site_url('Frontend/simpan')?>",
        type: "POST",
        data: formData,
        dataType: "JSON",
        success: function(response)
        {
            console.log(response);  
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
 
        }
        
    });
  }
</script>


<div class="container">
  <form action="#" id="form">

    <div id="soal_bagian_1" style="display: none;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 1</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>

          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil1" value="1">1 
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil1" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil2" value="9">1 
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no1_pil2" style="margin-left: 5px;" value="1">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil3" value="1">1 
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil3" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil4" value="1">1 
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil4" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no1_pil5" value="1">1 
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no1_pil5" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 2</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil1" value="9">1 
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil1" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil2" value="9">1 
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil2" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil3" value="9">1 
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil3" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil4" value="9">1 
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil4" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no2_pil5" value="9">1 
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no2_pil5" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="soal_bagian_2" style="display: none;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 3</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>

          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no3_pil1" value="1">1 
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no3_pil1" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no3_pil2" value="9">1 
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no3_pil2" style="margin-left: 5px;" value="1">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no3_pil3" value="1">1 
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no3_pil3" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no3_pil4" value="1">1 
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no3_pil4" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no3_pil5" value="1">1 
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no3_pil5" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 4</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no4_pil1" value="9">1 
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no4_pil1" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no4_pil2" value="9">1 
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no4_pil2" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no4_pil3" value="9">1 
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no4_pil3" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no4_pil4" value="9">1 
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no4_pil4" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no4_pil5" value="9">1 
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no4_pil5" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="soal_bagian_3" style="display: none;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 5</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>

          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no5_pil1" value="1">1 
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no5_pil1" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no5_pil2" value="9">1 
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no5_pil2" style="margin-left: 5px;" value="1">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no5_pil3" value="1">1 
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no5_pil3" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no5_pil4" value="1">1 
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no5_pil4" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no5_pil5" value="1">1 
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no5_pil5" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 6</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no6_pil1" value="9">1 
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no6_pil1" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no6_pil2" value="9">1 
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no6_pil2" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no6_pil3" value="9">1 
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no6_pil3" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no6_pil4" value="9">1 
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no6_pil4" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no6_pil5" value="9">1 
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no6_pil5" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="soal_bagian_4" style="display: none;">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 7</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>

          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no7_pil1" value="1">1 
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no7_pil1" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no7_pil2" value="9">1 
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no7_pil2" style="margin-left: 5px;" value="1">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no7_pil3" value="1">1 
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no7_pil3" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no7_pil4" value="1">1 
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no7_pil4" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>
            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no7_pil5" value="1">1 
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="2">2 
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="3">3
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="4">4
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="6">6
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="7">7
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="8">8
              <input type="radio" name="no7_pil5" style="margin-left: 5px;" value="9">9
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Soal no 8</h3>
        </div>

        <!-- content -->
        <div class="box-body">
          <textarea class="form-control textarea_era" rows="3" placeholder="" disabled>Anda menghadapi masalah tata kelola internal perusahaan yang tidak eektif dan efisien sehingga berpengaruh negatif pada bisnis secara keseluruhan. Semantara itu, komisaris PT Askrindo (Persero) mengiginkan lompatan kemajuan yang anda yakini sulit jika diwujudkan dengan stuasi regulasi perusahaan saat ini. Bagaimana Anda menyikapinya?</textarea>
          <br>
          Masalah tersebut merupakan sesuatu yang?
          <br>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no8_pil1" value="9">1 
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no8_pil1" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no8_pil2" value="9">1 
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no8_pil2" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no8_pil3" value="9">1 
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no8_pil3" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no8_pil4" value="9">1 
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no8_pil4" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-xs-4">
              <!-- <textarea class="form-control" rows="3" placeholder="" disabled>Di luar kendali</textarea> -->
              <input type="text" class="form-control" placeholder="Di luar kendali" disabled>
            </div>

            <div class="col-xs-4" style="text-align: center;">
              <input type="radio" name="no8_pil5" value="9">1 
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="8">2 
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="7">3
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="6">4
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="5">5
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="4">6
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="3">7
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="2">8
              <input type="radio" name="no8_pil5" style="margin-left: 5px;" value="1">9
            </div>

            <div class="col-xs-4">
              <input type="text" class="form-control" placeholder="Menjadi bagian yang harus saya kendalikan" disabled>
            </div>
          </div>
        </div>
      </div>
    </div>

  </form> 
  <div class="box-footer">
    <button type="submit" class="btn btn-danger" id="btn_kebali" onclick="prev()" style="display: none;">Kembali</button>
    <button type="submit" class="btn btn-warning" id="btn_selanjutnya" onclick="next()" style="display: none;">Soal selanjutnya</button>
    <button type="submit" class="btn btn-success" id="btn_simpan" onclick="simpan()" style="display: none;">simpan</button>
  </div>
</div>