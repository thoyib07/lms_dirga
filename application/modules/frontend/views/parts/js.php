<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> -->
<!--<script src="<?php echo base_url();?>assets/admin/js/jquery-3.1.1.min.js"></script>-->
<script src="<?php echo base_url();?>assets/js/jquery.flexslider.js"></script>

<script src="<?php echo base_url('assets/js/'); ?>highcharts.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>exporting.js"></script>

<script src="<?php echo base_url();?>assets/admin/js/pace.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/classie.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/cbpAnimatedHeader.js"></script>
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>maps_api.js" async defer></script>
<!--
<script src="<?php echo base_url();?>assets/admin/js/plugins/chartJs/Chart.min.js"></script>
-->

<script>
function ajax(id, data, funct){
	var urls = "<?php  echo base_url(); ?>chart/"+id+"/open";
	$.ajax({
		url : urls,
		type: "POST",
		dataType: 'json',
		success: function(data){
			lineChartData = data;//alert(JSON.stringify(data));
			var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);

			var ctx = document.getElementById("canvas").getContext("2d");
			window.myLine = new Chart(ctx).Line(lineChartData, {responsive: true});
			console.log(urls);
			console.log(datas);
			//funct(datas);
		}
	});
}

function ktda(id) {
	$('#content').html("");
		$.ajax({
		    type:'post',
		    url:'<?php echo base_url('frontend/ktda/')?>',// put your real file name 
		    data:{id: id},
		    success:function(msg){
		    	$('#content').append(msg);	    	
		    }
		});
}

function kcda(id) {
	$('#content').html("");
		$.ajax({
		    type:'post',
		    url:'<?php echo base_url('frontend/kcda/')?>',// put your real file name 
		    data:{id: id},
		    success:function(msg){
		    	$('#content').append(msg);	    	
		    }
		});
}

function pdrb(id) {
	$('#content').html("");
		$.ajax({
		    type:'post',
		    url:'<?php echo base_url('frontend/pdrb/')?>',// put your real file name 
		    data:{id: id},
		    success:function(msg){
		    	$('#content').append(msg);	    	
		    }
		});
}

function generateChart(id, target){
	var data = {};
	var target = '#'+target;
	
	spinner(target);
	
	function ajax_return(datas){
		chart(target, datas.type+'Chart', datas.title, datas.description, datas.shell)
	}
	
	ajax(id, data, ajax_return);
}

function chartOption(type, data){
	var options = {};
	if(type == 'lineChart'){
		options = {
			scaleShowGridLines: true,
			scaleGridLineColor: "rgba(0,0,0,.05)",
			scaleGridLineWidth: 1,
			bezierCurve: true,
			bezierCurveTension: 0.4,
			pointDot: true,
			pointDotRadius: 4,
			pointDotStrokeWidth: 1,
			pointHitDetectionRadius: 20,
			datasetStroke: true,
			datasetStrokeWidth: 2,
			datasetFill: true,
			responsive: true,
		};
	}else if(type == 'doughnutChart'){
		options = {
			segmentShowStroke: true,
			segmentStrokeColor: "#fff",
			segmentStrokeWidth: 2,
			percentageInnerCutout: 45, // This is 0 for Pie charts
			animationSteps: 100,
			animationEasing: "easeOutBounce",
			animateRotate: true,
			animateScale: false,
			responsive: true,
		};
	}else if(type == 'barChart'){
		options = {
			scaleBeginAtZero: true,
			scaleShowGridLines: true,
			scaleGridLineColor: "rgba(0,0,0,.05)",
			scaleGridLineWidth: 1,
			barShowStroke: true,
			barStrokeWidth: 2,
			barValueSpacing: 5,
			barDatasetSpacing: 1,
			responsive: true,
		}
	};
	
	return options;
}

function spinner(target){
	var target = $(target);
	var view = '<img src="<?php echo base_url();?>/assets/spinner.gif" class="" style="width:50%;">';
	target.empty();
	
	target.append(view);
}

function chart(target, type, title, description, datas){
	$(target).empty();
	var width = $(target).width();
	$(target).append('<canvas id="'+type+'_chart" height="300" width="'+width+'"></canvas>');
	$(target).append('<h5>'+title+'</h5>');
	$(target).append('<span>'+description+'</span>');
	var chartType = type.replace('Chart','');
		chartType = chartType.charAt(0).toUpperCase() + chartType.slice(1).toLowerCase();
		
	var ctx = document.getElementById(type+'_chart').getContext("2d");
	
	var data = {};
	
	if(type == 'lineChart'){
		data = JSON.parse(datas);
	}else if(type == 'doughnutChart'){				
		data = JSON.parse(datas);
	}else if(type == 'barChart'){
		data = JSON.parse(datas);
	}
	
	new Chart(ctx)[chartType](data, chartOption(chartType, data));
}

$(document).ready(function () {
	$( ".search_cancel" ).click(function() {
	  $( ".search_field" ).toggle( "slow", function() {
		  
	  });
	});
	$( ".search" ).click(function() {
	  $( ".search_field" ).toggle( "slow", function() {
	  });
	});
	
    // Highlight the top nav as scrolling
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 80
    })

    // Page scrolling feature
    $('a.page-scroll').bind('click', function(event) {
        var link = $(this);
        $('html, body').stop().animate({
            scrollTop: $(link.attr('href')).offset().top - 70
        }, 500);
        event.preventDefault();
    });

    $(function(){
      //SyntaxHighlighter.all();
    });

    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
    /*
	var mapbg = new GMaps({
		div: '#gmapbg',
		lat: -6.176670,
		lng: 106.654829,
		scrollwheel: false,
		zoom: 11
	});


	mapbg.addMarker({
		lat: -6.176670,
		lng: 106.654829,
		title: 'Your Location',
		infoWindow: {
			content: '<h5>Tangerang</h5><p></p>'
		}
	});
	*/
	setTimeout(function(){
		var firstChart = $('#line_type').val();
		if(firstChart){
			generateChart(firstChart, 'lineChart');
		}
		
		var secondChart = $('#pie_type').val();
		if(secondChart){
			generateChart(secondChart, 'pieChart');
		}
	}, 200);
	
	$('#pie_type').change(function(){
		var value = $(this).val();
		if(value){
			var value = $(this).val();
			generateChart(value, 'pieChart');
		}
	});
	
	$('#line_type').change(function(){
		var value = $(this).val();
		if(value){
			var value = $(this).val();
			generateChart(value, 'lineChart');
		}
	});
});

// Activate WOW.js plugin for animation on scrol
new WOW().init();
</script>