<!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.min.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/font-awesome/css/'); ?>font-awesome.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style2.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/flexslider.css" rel="stylesheet">
    <style>
       #maps {
        height: 400px;
        width: 100%;
       }
    </style>
	<style>
		.navbar.navbar-scroll .navbar-brand{
			margin-top: 0
		}
		.no-padding{
			padding:0px !important;
		}
		.control-label{
			margin-top: 8px;
		}
		.filtering{
			margin-top: -50px;
			background: #fff;
			position: relative;
			padding-top: 15px;
		}
		.navbar-default .navbar-nav>.open>a{
			padding-bottom: 29px;
			border-color: #1ab394;
			background: #fff;
		}
		.open>.dropdown-menu{
			border-radius: 0;
		}
		.dropdown-menu>li>a{
			color: #000 !important;
		}
		/*
		.dropdown-menu{
			//max-height:400px;
			//overflow-y: scroll;
		}
		*/
		.dropdown-submenu {
			position:relative;
			width: 100%;
		}
		.dropdown-submenu>.dropdown-menu {
			top:0;
			left:100%;
			margin-top:-6px;
			margin-left:-1px;
		}
		.dropdown-submenu:hover>.dropdown-menu {
			display:block;
		}
		.dropdown-submenu>a:after {
			display:block;
			content:" ";
			float:right;
			width:0;
			height:0;
			border-color:transparent;
			border-style:solid;
			border-width:5px 0 5px 5px;
			border-left-color:#cccccc;
			margin-top:5px;
			margin-right:-10px;
		}
		.dropdown-submenu:hover>a:after {
			border-left-color:#ffffff;
		}
		.dropdown-submenu.pull-left {
			float:none;
		}
		.dropdown-submenu.pull-left>.dropdown-menu {
			left:-100%;
			right: 100%;
			margin-left:10px;
		}
		.navbar-default .nav li a{
			font-size: 12px;
		}
		.navbar-scroll.navbar-default .nav li a{
			color: #fff;
		}
		.header-back{
			background-size: cover !important;
		}
		.search_field{
			position: absolute;
			padding: 14px 100px 15px;
			margin-left: -15px;
			background: #fff;
			border-bottom: 1px solid #ccc;
		}
		.search_input{
			height: 50px;
			width:100%;
			border: 0;
		}
		.search_cancel{
			float: right;
			top: 10px;
			position: absolute;
			right: 25px;
			font-size: 35px;
		}
	@media (max-width:768px){
		.mobile-hide{
			display:none !important;
		}
		.navbar-brand > img{
			margin-right: 0px !important;
			padding-left: 5px;
			padding-right: 5px;
		}
		.navbar-toggle{
			padding: 28px;
			background-color: #fff;
			margin: 0;
			border: 0;
			border-radius: 0;
		}
		.navbar-default .navbar-collapse{
			background: #fff;
		}
		.navbar-default .nav li a{
			border-bottom:1px solid #fff;
		}
		.navbar-scroll.navbar-default .nav li a {
			color: #676a6c;
		}
		.navbar-nav{
			margin-top: 0px;
			margin-bottom: 0;
		}
		.navbar-default .nav li a{
			background: #fec809;
		}
		.search_field{
			top: 0;
			width: 100%;
			padding: 15px;
		}
	}	
	</style>