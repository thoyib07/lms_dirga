<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="Website Sistem Informasi Data Statistik Daerah Kota Tangerang">
	<meta name="keywords" content="tangerang, kota, tangerangkota, statistik, data,  investasi, SISTAD" />
	<meta name="title" itemprop="name" content="<?php echo (!$title) ? 'SISTAD' : $title; ?>" />
	<meta name="standout" content="<?php echo base_url(); ?>"/>
	<meta name="copyright" content="Dinas Informasi dan Komunikasi Kota Tangerang" /> 
  <meta name="author" content="Dinas Informasi dan Komunikasi Kota Tangerang - Pemerintah Kota Tangerang" />

  <meta name="robots" content="all" /> 
  <meta name="spiders" content="all" /> 
  <meta name="webcrawlers" content="all" />
  <meta name="language" content="Indonesia" />
  <meta name="revisit" content="1 days" /> 
  <meta name="distribution" content="global" /> 
	
	<meta name="rating" content="general" />
	<meta name="googlebot-news" content="index,follow" />
	<meta name="googlebot" content="all" />
	<meta name="MSSmartTagsPreventParsing" content="TRUE" />
	<meta name="generator" content="<?php echo base_url(); ?>" />
	<meta name="geo.position" content="-6.1780556, 106.63" />
	<meta name="geo.country" content="ID" />
	<meta name="geo.placename" content="Indonesia" />
	<meta content="index,follow" name="robots" />
	<meta name="theme-color" content="#1ab394">

	<link itemprop="mainEntityOfPage" href="<?php echo base_url(); ?>"/>
	<link rel="canonical" href="<?php echo base_url(); ?>" />

  <meta property="og:title" content="" /> 
  <meta property="og:url" content="<?php echo base_url(); ?>" /> 
  <!-- <meta property="og:image" content="assets/images/common/blank.jpg" /> -->
  <meta property="og:description" content="" /> 
  <meta property="fb:app_id"      content="816976845020258" /> 
  <meta property="og:type"        content="article" />
  
 
  <!-- Add Your favicon here -->
  <!--<link rel="icon" href="img/favicon.ico">-->

  <title><?php echo (!$title) ? 'SISTAD' : $title; ?></title>
  <link rel="shortcut icon" href="<?php echo base_url('assets/img/'); ?>favicon.ico" type="image/x-icon" />

  <?php include 'parts/css.php'; ?>
  <script src="<?php echo base_url();?>assets/admin/js/jquery-2.1.1.js"></script>
  <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/js/'); ?>jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/js/'); ?>dataTables.bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-20936964-7']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
 </script>
    
</head>
<body id="page-top">

<?php include 'parts/head.php'; ?>

<div id="content">
<?php echo (!$content) ? 'SISTAD' : $content; ?>  
</div>

<?php include 'parts/foot.php'; ?>

<?php include 'parts/js.php'; ?>


</body>
</html>