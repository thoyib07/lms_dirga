<section class="container services">
    <div class="row">
        <div class="col-sm-12">
	        <div class="flexslider">
			  <ul class="slides">
			  	<?php foreach ($slider as $s) { ?>
			  		<?php if (isset($s->image)) { ?>
			  			<li>
					      <img src="<?php echo base_url('assets/uploads/slider/').''.$s->image;?>" title="<?php echo $s->nama; ?>"/>
					    </li>
			  		<?php } ?>
				<?php } ?>
			  </ul>
			</div>
        </div>
        <!--
        <div class="col-sm-3">
			<select id="pie_type" class="form-control m-b" style="border-radius: 0;">
				<?php /*  foreach($graph as $g){ ?>
					<?php if($g->type == 'doughnut'){ ?>
					<option value="<?php echo $g->id;?>"><?php echo $g->nama;?></option>
					<?php } ?>
				<?php } */  ?>
			</select>
			<div id="pieChart" class="col-xs-12 no-padding">
				<canvas id="pie-graph" width="1000" height="400"></canvas>
			</div>
        </div>
        -->
    </div>
</section>

<section id="maps" class="gray-section team" style="height:400px">
    <div class="container">
	
    </div>
</section>
<script>
      function initMap() {
        var uluru = {lat: -6.169927, lng: 106.640290};
        var map = new google.maps.Map(document.getElementById('maps'), {
         	zoom: 11,
          	center: uluru,
          	zoomControl: false,
    		scaleControl: false,
    		streetViewControl: false,
    		scrollwheel: false
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
</script>

<section class="features">
    <div class="container">
    	<?php if (false): ?>
		<div class="filtering row m-b-lg">
            <div class="col-lg-12">
                <div class="col-md-1 no-padding">
					<h4 class="text-center">Filter: </h4>
				</div>
                <div class="col-md-11 no-padding">
					<div class="col-md-12 no-padding">
						<div class="col-sm-12">
							<select id="line_type" class="form-control m-b" style="border-radius: 0;">
								<?php  foreach($graph as $g){ ?>
									<?php if(($g->type == 'line')||($g->type == 'bar')){ ?>
									<option value="<?php echo $g->id;?>"><?php echo $g->nama;?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
            </div>
        </div>
        <div class="row features-block">
			<div id="lineChart" class="col-lg-12 wow fadeInLeft">
			
            </div>
        </div>
    	<?php endif ?>
    </div>

</section>