<?php 
// if (isset($user_akses)) {
//   $hak_akses = array();
//   foreach ($user_akses as $ua) {
//     array_push($hak_akses, $ua->ktda_id);
//   }
// }
// var_dump($hak_akses);
?>
<!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- search form -->
      <!--
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li id="menu_home" class="menu treeview <?php if (strtolower($page['p']) == "home") { echo "active"; } ?>">
          <a href="#">
            <i class="fa fa-home"></i> <span>Home</span>
            <?php if (isSuper()): ?>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            <?php endif ?>
          </a>
          <?php if (isSuper()): ?> 
          <ul class="treeview-menu">
            <li <?php if (strtolower($page['c']) == "slider") { echo "class='active'"; } ?>><a href="<?php echo base_url().'admin/slider'; ?>"><i class="fa fa-image"></i> Slider</a></li>

            <?php if (false): ?>
            <li <?php if (strtolower($page['c']) == "pie") { echo "class='active'"; } ?>><a href="<?php echo base_url('admin/pie'); ?>"><i class="fa fa-pie-chart"></i> Pie Chart</a></li>
            <li <?php if (strtolower($page['c']) == "line") { echo "class='active'"; } ?>><a href="<?php echo base_url('admin/line'); ?>"><i class="fa fa-line-chart"></i> line Chart</a></li>
            <?php endif ?>            
          </ul>            
          <?php endif ?>
        </li>

        <?php if (isSuper()): ?>
        <li id="menu_user" class="menu <?php if (strtolower($page['p']) == "user") { echo "active"; } ?>">
          <a href="<?php echo base_url('admin/user'); ?>"  title="Daftar User">
            <i class="fa fa-user"></i> <span>User</span>
          </a>
        </li>
        <?php endif ?>

                
        <!--
        <li class="treeview <?php // if (strtolower($page['p']) == "pdrb") { echo "active"; } ?>">
          <a href="#" title="Data Produk Domestik Regional Bruto">
            <i class="fa fa-table"></i> <span>Data PDRB</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          -->
          <!--
          <ul class="treeview-menu" >
          <?php /* foreach ($ktda as $ktda) { ?>
            <li <?php if ($page['c'] == $ktda->permalink) { echo "class = 'active'"; } ?>><a href="<?php echo base_url().'admin/ktda/'.$ktda->permalink; ?>" style="font-size: 10px;" ><i class="fa fa-table"></i> <?php echo $ktda->judul; ?></a></li>
          <?php } */?>
          </ul>          
        </li>
        -->        

        <!--
        <li>
          <a href="<?php // echo base_url('admin/statistik/kcda'); ?>" title="Tambah Menu Statistik Kecamatan">
            <i class="fa fa-plus"></i> <span>Menu Statistik Kecamatan</span>
          </a>
        </li>
        -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>