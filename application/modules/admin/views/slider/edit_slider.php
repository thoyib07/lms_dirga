<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Sistem Informasi Statistik Daerah
        <small>SISTAD</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> <?php echo ucwords($page['p']); ?></a></li>
        <li class="active"><?php echo ucwords($page['c']); ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h1 class="box-title">Ubah Data Slider</h3>
        </div>
        <?php foreach ($slider as $s) { ?>
        <form action="" method="post" role="form" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="judul">Judul</label>
              <input class="form-control" id="judul" placeholder="Judul Slider" type="text" name="judul" value="<?php echo $s->nama; ?>">
            </div>
            <div class="form-group">
              <label for="gambar_baru">Gambar Baru</label>
              <input id="gambar_baru" type="file" name="gambar_baru">
              <?php if ($s->image != "") { ?>
                <hr>
                <label for="gambar_lama">Gambar Lama</label><br>
                <input type="hidden" name="gambar_lama" value="<?php echo $s->image; ?>">
                <input type="hidden" name="baru" value="<?php if ($s->image == "") { echo "1"; } else { echo "0"; } ?>">
                <input type="hidden" name="datecreated" value="<?php echo $s->datecreated; ?>">
                <img src="<?php echo base_url('assets/uploads/slider/').''.$s->image; ?>"  width="20%">.
              <?php } ?>
            </div>
            <div class="form-group">
              <label for="gambar">Status : </label>
              <?php if ($s->status == 1) { ?>
                <font color="Grean"><h5>Aktif</h5></font>
              <?php } else { ?>
                <font color="Red"><h5>Tidak Aktif</h5></font>
              <?php } ?>
              <h5>NB : Status akan berubah menjadi <font color="grean">AKTIF</font> setelah data dikirim.</h5>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" name="submit" value="1" class="btn btn-success fa fa-save" title="Simpan"></button>
            <a href="<?php echo $back; ?>"><button type="button" class="btn btn-warning fa fa-arrow-left pull-right" title="Kembali"></button></a>
          </div>
        </form>
        <?php } ?>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->