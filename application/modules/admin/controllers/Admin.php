<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index() {
		if(!isset($_SESSION)) { session_start(); }

		active_time();
		if ($this->session->userdata('cms_login') == 1) {
			//var_dump('masuk');
			redirect(base_url().'admin/home');
		} else {
			redirect(base_url().'auth/login');
		}
	}

	public function home() {
		active_time();
		$data['page'] 	= array('p' => '', 'c' => '' );
		$data['title'] 	= 'LMS Dirga';

		$data['user'] = info_user();
		// var_dump($data['user']);die();
		// var_dump("<hr>",$data['ktda']);
		$data['content'] 	= $this->load->view('home',$data,TRUE);
		$this->load->view('layout',$data);
	}

	public function slider($action=null,$id=null) {
		active_time();
		if (isSuper()) {		
			$data['page'] 	= array('p' => 'home', 'c' => 'slider' );

			$data['user'] = info_user();
			//menu		
			$data = array_merge($data,side_menu());

			$data['back']	= base_url('admin/slider');

			switch ($action) {
				case 'add':
					if (isset($_POST['submit'])) {
						
						//Upload file

						//Save to DB
						//$value = array('nama' => $_POST['judul'], 'image' => $image, 'created_date' => date('Y-m-d H:i:s'));
						die();
					} else {
						$data['title'] 	= 'Admin SISTAD - Tambah Slider';
						$data['content'] 	= $this->load->view('slider/add_slider',$data,TRUE);
					}			
				break;

				case 'edit':
					if (isset($_POST['submit'])) {
						$data['id'] = $id;
						$data['nama'] = $_POST['judul'];
						if ($_POST['baru']) {
							$data['datecreated'] = date('Y-m-d H:i:s');
						} else {
							$data['datecreated'] = $_POST['datecreated'];
							$data['dateupdated'] = date('Y-m-d H:i:s');
						}
						$data['status'] = "1";

						$name = "gambar_baru";
						$new_name = time();
						if ($_FILES[$name]['name'] == "") {
							$data['image'] = $_POST['gambar_lama'];
						} else {
							$type = "gif|jpg|png|jpeg";
							$path = "slider";
							switch ($_FILES[$name]['type']) {
							case "image/jpeg" :
								$real_name = str_replace([".jpg",".jpeg",".JPG",".JPEG"],"",$_FILES[$name]['name']);
								$real_name = str_replace(" ","_",$real_name);
								$nama_file = $real_name."-".$new_name.'.jpeg';
								break;
							case "image/png" :
								$real_name = str_replace([".png",".PNG"],"",$_FILES[$name]['name']);
								$real_name = str_replace(" ","_",$real_name);
								$nama_file = $real_name."-".$new_name.'.png';
								break;
							case "image/gif" :
								$real_name = str_replace([".gif",".GIF"],"",$_FILES[$name]['name']);
								$real_name = str_replace(" ","_",$real_name);
								$nama_file = $real_name."-".$new_name.'.gif';
								break;
							default:
								$real_name = str_replace(" ","_",$_FILES[$name]['name']);
								$nama_file = $real_name;
								break;
							}
							$data['image']  = unggah_berkas($path,$nama_file,$type);
							$this->upload->do_upload($name);
						}
						//var_dump($data);die();
						$this->m_admin->editSlider($data);
						redirect(base_url('admin/slider'));
					} else {
						$data['title'] 		= 'Admin SISTAD - Ubah Slider';
						$slider['select']	= "*";
						$slider['table']	= "slider";
						$slider['where'] 	= "id = '".$id."'";
						$data['slider'] 	= $this->m_admin->getAll($slider);

						$data['content'] 	= $this->load->view('slider/edit_slider',$data,TRUE);
					}
				break;

				case 'delete':
					$data['title'] 		= 'Admin SISTAD - Hapus Slider';
					$slider['id']		= $id;
					$slider['table']	= "slider";
					$data['slider'] 	= $this->m_admin->delById($slider);

					redirect(base_url('admin/slider'));
				break;

				case 'restore':
					$data['id']		= $id;
					$data['table']	= "slider";

					$this->m_admin->restorById($data);
					redirect('admin/slider');
				break;
				
				default:
					$data['title'] 	= 'Admin SISTAD - Slider';
					$slider['select']	= "*";
					$slider['table']	= "slider";
					$data['slider'] 	= $this->m_admin->getAll($slider);

					$data['content'] 	= $this->load->view('slider',$data,TRUE);
				break;
			}
			
			$this->load->view('layout',$data);
		} else {
			redirect(base_url('admin'));
		}
	}

	public function user($action=null,$id=null) {
		active_time();
		if (!isSuper()) { redirect(base_url('admin')); } // Jika bukan super admin maka dilempar ke halaman home admin
		$data['page'] 	= array('p' => 'user', 'c' => '' );

		$data['user'] = info_user();
		//menu		
		$data = array_merge($data,side_menu());

		$data['back']	= base_url('admin/user');
		$data['sub_ktda'] = all_sub_ktda();
		// var_dump("<hr>",$data['m_kecamatan']);
		switch ($action) {
			case 'tambah':
				if (isset($_POST['submit'])) {
					// var_dump($_POST);
					// die();
					$link = 'http://opendatav2.tangerangkota.go.id/services/pegawai/pegawaibynip/nip/'.$_POST['nip'];
					$this->curl->create($link); 

					$this->curl->http_login(REST_U, REST_P); // login open data	
					$result = json_decode($this->curl->execute(), true);
					if (!$result) {
						// var_dump("<hr>",$link);
						// var_dump("<hr>",$result);
						// die();
						$this->session->set_flashdata('error','NIP yang dimasukan salah!!!');
  						redirect('admin/user/tambah');
					}

					$user['user_type_id'] 	= $_POST['hak_akses'];
					$user['kode_unor'] 		= $result['kode_unor'];
					$user['nip'] 			= $result['nip_baru'];
						
					// if ( ($result['gelar_depan'] !== NULL) || ($result['gelar_depan'] !== "") || ($result['gelar_depan'] !== "-") ) {
					// 	$user['nama'] 			= $result['gelar_depan'].'. '.$result['nama_pegawai'].' '.$result['gelar_belakang'];
					// } else {
					// 	$user['nama'] 			= $result['nama_pegawai'].' '.$result['gelar_belakang'];
					// }
					$user['nama'] 		= $result['nama_pegawai'].' '.$result['gelar_belakang'];

					$data['select'] 	= "id";
					$data['table']		= "user";
					$data['where']		= "nip = '".$user['nip']."'";
					$result = $this->m_admin->getAll($data);
					if ($result) {
						$this->session->set_flashdata('error','NIP sudah terdaftar di aplikasi ini!!!');
  						redirect('admin/user/tambah');
					} else {
						$data['data'] 	= array('user_type_id' => $user['user_type_id'], 'kode_unor' => $user['kode_unor'], 'nip' => $user['nip'], 'nama' => $user['nama'], 'regis_date' => date('Y-m-d H:i:s'), 'status' => '1');
  						$data['table']	= "user";
  						
  						$id_user = $this->m_admin->addData($data);

  						if ($user['user_type_id'] == 1) {
  							$data['sub_ktda']	= menu_all_sub_ktda();
  							$data['table']		= "user_akses";
  							foreach ($data['sub_ktda'] as $sk) {
  								$data['data'] 	= array('ktda_id' => $sk->id, 'user_id' => $id_user, 'status' => '1');
  						
  								$this->m_admin->addData($data);
  							}

  							foreach ($data['m_kecamatan'] as $kec) {
  								$data['data'] 	= array('kecamatan_id' => $kec->id, 'user_id' => $id_user, 'status' => '1');
  						
  								$this->m_admin->addData($data);
  							}
  						} else {
  							//bukan super admin
  							$data['table']	= "user_akses";
  							foreach ($_POST['sub_ktda'] as $sk) {
  								$data['data'] 	= array('ktda_id' => $sk, 'user_id' => $id_user, 'status' => '1');
  						
  								$this->m_admin->addData($data);
  							}

  							foreach ($_POST['kecamatan'] as $kec) {
  								$data['data'] 	= array('kecamatan_id' => $kec->id, 'user_id' => $id_user, 'status' => '1');
  						
  								$this->m_admin->addData($data);
  							}
  						}  						
					}
					redirect('admin/user');
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'user', 'c' => 'tambah user' );
					$data['title'] 	= 'SISTAD';
					$data['content'] 	= $this->load->view('user/tambah_user',$data,TRUE);
				}
			break;

			case 'ubah':
				$user['select']	= "u.*";
				$user['table']	= "user as u";
				$user['where']	= "u.id = '".$id."' and u.status = 1";
				$data['user'] 	= $this->m_admin->getAll($user);

				$data['user_akses_ktda'] 		= hak_akses_ktda($data['user']['0']->id);
				$data['user_akses_kecamatan'] 	= hak_akses_kecamatan($data['user']['0']->id);
					
				if (isset($_POST['submit'])) {
					$user_akses_ktda_array = array();
					foreach ($data['user_akses_ktda'] as $ua) {
						array_push($user_akses_ktda_array, $ua->ktda_id);
					}

					$user_akses_kecamatan_array = array();
					foreach ($data['user_akses_kecamatan'] as $ua) {
						array_push($user_akses_kecamatan_array, $ua->kecamatan_id);
					}
					$user_akses_update['table']	= "user_akses";
					// var_dump("<hr>",$data['user']);
					// var_dump("<hr>",$data['user_akses_ktda']);
					// var_dump("<hr>",$_POST);
					// var_dump("<hr>",$user_akses_ktda_array);
					// die();
					if ($_POST['hak_akses'] == $data['user'][0]->user_type_id) {
						//tidak ada perubahan hak akses
						//jika hak akses operator ada penambahan atau pengurangan
						if ($_POST['hak_akses'] == 2) {
							//cari $_POST['sub_ktda'] tidak ada di $data['user_akses_ktda'] (tambah)
							// $array_tambah = array_diff($_POST['sub_ktda'], $user_akses_ktda_array);
							// var_dump("<hr> KTDA",$_POST['sub_ktda']);
							foreach ($_POST['sub_ktda'] as $tambah) {
								$user_akses['select']		= "u.*";
								$user_akses['table']		= "user_akses as u";
								$user_akses['where']		= "u.user_id = '".$id."' and u.ktda_id = '".$tambah."'";
								$data['user_akses_ktda'] 	= $this->m_admin->getAll($user_akses);
								// var_dump("<hr> Tambah",$tambah);
								// var_dump("<hr> user_akses",$user_akses);
								// var_dump("<hr> Hasil",$data['user_akses_ktda']);
								if ($data['user_akses_ktda']) {
									$user_akses_update['data'] 	= array('status' => '1');
	  								$user_akses_update['where'][0] = array('ktda_id', $tambah);
	  								$user_akses_update['where'][1] = array('user_id', $id);
	  								$this->m_admin->updateData($user_akses_update);
								} else {
									$user_akses_update['data'] 	= array('ktda_id' => $tambah, 'user_id' => $id, 'status' => '1');
  						
  									$this->m_admin->addData($user_akses_update);
								}								
							}
							//cari $data['user_akses_ktda'] tidak ada di $_POST['sub_ktda'] (kurang)
							$array_kurang = array_diff($user_akses_ktda_array, $_POST['sub_ktda']);
							var_dump("<hr>",$user_akses_ktda_array);
							var_dump("<hr>",$_POST['sub_ktda']);
							var_dump("<hr>",$array_kurang);
							die();
							foreach ($array_kurang as $kurang) {
								$user_akses_update['data'] 	= array('status' => '0');
  								$user_akses_update['where'][0] = array('ktda_id', $kurang);
  								$user_akses_update['where'][1] = array('user_id', $id);
  								$this->m_admin->updateData($user_akses_update);
							}

							//cari $_POST['kecamatan'] tidak ada di $data['user_akses_kecamatan'] (tambah)
							// $array_tambah = array_diff($_POST['kecamatan'], $user_akses_kecamatan_array);
							var_dump("<hr> kecamatan",$_POST['kecamatan']);
							foreach ($_POST['kecamatan'] as $tambah) {
								$user_akses['select']	= "u.*";
								$user_akses['table']	= "user_akses as u";
								$user_akses['where']	= "u.user_id = '".$id."' and u.kecamatan_id = '".$tambah."'";
								$data['user_akses_kecamatan'] 	= $this->m_admin->getAll($user_akses);
								if ($data['user_akses_kecamatan']) {
									$user_akses_update['data'] 	= array('status' => '1');
	  								$user_akses_update['where'][0] = array('kecamatan_id', $tambah);
	  								$user_akses_update['where'][1] = array('user_id', $id);
	  								$this->m_admin->updateData($user_akses_update);
								} else {
									$user_akses_update['data'] = array('kecamatan_id' => $tambah, 'user_id' => $id, 'status' => '1');
  						
  									$this->m_admin->addData($user_akses_update);
								}
							}
							//cari $data['user_akses_kecamatan'] tidak ada di $_POST['kecamatan'] (kurang)
							$array_kurang = array_diff($user_akses_kecamatan_array, $_POST['kecamatan']);
							foreach ($array_kurang as $kurang) {
								$user_akses_update['data'] 	= array('status' => '0');
  								$user_akses_update['where'][0] = array('kecamatan_id', $kurang);
  								$user_akses_update['where'][1] = array('user_id', $id);
  								$this->m_admin->updateData($user_akses_update);
							}
							// var_dump("<hr>",$array_tambah);
							// var_dump("<hr>",$array_kurang);
							// die();
						}
					} else {
						//hak akses tidak sama dengan yang terdahulu
						if ($_POST['hak_akses'] == 1) {
							//penambahan / Ubah hak akses jika sebelumnya operator
							$data['sub_ktda']	= menu_all_sub_ktda();
  							$data['table']	= "user_akses";
  							//cek KTDA
  							foreach ($data['sub_ktda'] as $sk) {
  								$user_akses['select'] 	= "id";
								$user_akses['table']	= "user_akses";
								$user_akses['where']	= "user_id = '".$id."' and ktda_id = '".$sk->id."'";
								$data['user_akses_ktda'] 	= $this->m_admin->getAll($user_akses);
  								if ($data['user_akses_ktda']) {
  									// data sudah ada di DB
  									// echo "<hr> Tidak Aktif";
  									$user_akses_update['data'] 	= array('status' => '1');
	  								$user_akses_update['where'][0] = array('ktda_id', $sk->id);
	  								$user_akses_update['where'][1] = array('user_id', $id);
	  								$this->m_admin->updateData($user_akses_update);
  								} else {
  									//belum ada di DB
  									// echo "<hr> Aktif";
  									$data['data'] 	= array('ktda_id' => $sk->id, 'user_id' => $id, 'status' => '1');
  									$this->m_admin->addData($data);
  								}
  							}
  							//cek Kecamatan
  							foreach ($data['m_kecamatan'] as $kec) {
  								$user_akses['select'] 	= "id";
								$user_akses['table']	= "user_akses";
								$user_akses['where']	= "user_id = '".$id."' and kecamatan_id = '".$kec->id."'";
								$data['user_akses_kecamatan'] 	= $this->m_admin->getAll($user_akses);
  								if ($data['user_akses_kecamatan']) {
  									// data sudah ada di DB
  									// echo "<hr> Tidak Aktif";
  									$user_akses_update['data'] 	= array('status' => '1');
	  								$user_akses_update['where'][0] = array('kecamatan_id', $kec->id);
	  								$user_akses_update['where'][1] = array('user_id', $id);
	  								$this->m_admin->updateData($user_akses_update);
  								} else {
  									//belum ada di DB
  									// echo "<hr> Aktif";
  									$data['data'] 	= array('kecamatan_id' => $kec->id, 'user_id' => $id, 'status' => '1');
  									$this->m_admin->addData($data);
  								}
  							}

						} else {
							//pengurangan hak akses jika sebelumnya admin
							$array_kurang = array_diff($user_akses_ktda_array, $_POST['sub_ktda']);
							foreach ($array_kurang as $kurang) {
								$user_akses_update['data'] 	= array('status' => '0');
								$user_akses_update['where'][0] = array('ktda_id', $kurang);
								$user_akses_update['where'][1] = array('user_id', $id);
								$this->m_admin->updateData($user_akses_update);
							}

							$array_kurang = array_diff($user_akses_kecamatan_array, $_POST['kecamatan']);
							foreach ($array_kurang as $kurang) {
								$user_akses_update['data'] 	= array('status' => '0');
								$user_akses_update['where'][0] = array('kecamatan_id', $kurang);
								$user_akses_update['where'][1] = array('user_id', $id);
								$this->m_admin->updateData($user_akses_update);
							}
						}
					}
					
					$data['data'] 		= array('user_type_id' => $_POST['hak_akses'], 'kode_unor' => $data['user'][0]->kode_unor, 'nip' => $_POST['nip'], 'nama' => $_POST['nama'], 'regis_date' => $data['user'][0]->regis_date, 'status' => '1');
  					$data['table']		= "user";
					$data['where'][0] 	= array('id', $id);
  					$this->m_admin->updateData($data); 
					// die();
  					redirect('admin/user');
				} else {
					//var_dump($data['user']);
					$data['page'] 	= array('p' => 'user', 'c' => 'ubah user' );
					$data['title'] 	= 'SISTAD';
					$data['content'] 	= $this->load->view('user/ubah_user',$data,TRUE);
				}
			break;

			case 'hapus':
				$data['id']		= $id;
				$data['table']	= "user";

				$this->m_admin->delById($data);
				redirect('admin/user');			
			break;

			case 'restore':
				$data['id']		= $id;
				$data['table']	= "user";

				$this->m_admin->restorById($data);
				redirect('admin/user');
			break;
			
			default:
				$data['title'] 		= 'Admin SISTAD - User';

				// $user['select']		= "u.*, s.nama";
				// $user['table']		= "user as u";
				// $user['join'][0]	= array('skpd as s','u.kode_unor like "%kode_unor%"' );

				// $data['list_user'] 	= $this->m_admin->getAll($user);

				$data['page'] 		= array('p' => 'user', 'c' => '' );
				$data['title'] 		= 'SISTAD';

				$data['content'] 	= $this->load->view('user',$data,TRUE);
				// echo $data['content'];
			break;
		}
		
		$this->load->view('layout',$data);
	}

	public function ajax_list() {
		$data 	= array();
        $no 	= $_POST['start'];
        $type 	= $_GET['type'];
        if (isset($_GET['id'])) {
        	$id 	= $_GET['id'];
        }
        switch ($type) {
        	case 'user':
        	case 'del_user':
        		$list = $this->m_admin->get_datatables($type);
        		// var_dump($list);
        		// die();
    			foreach ($list as $l) {
		            $no++;
		            $row = array();
		            $row[] = $no;
		            $row[] = $l->nama;
		            $row[] = $l->SKPD;
		            $row[] = $l->jabatan;
		            

		            if ($type == 'user') {
		            	$row[] = '<a href="'.base_url('admin/user/ubah').'/'.$l->id.'"><button class="btn btn-warning fa fa-pencil" type="button" title="Ubah Data User"></button></a>'.''.'<a href="'.base_url('admin/user/hapus').'/'.$l->id.'" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" ><button class="btn btn-danger fa fa-power-off" type="button" title="Nonaktifkan Data User"></button></a>';
		            } else {
		            	 $row[] = '<a href="'.base_url('admin/user/restore').'/'.$l->id.'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success fa fa-recycle" type="button" title="Aktifkan Data User"></button></a>';
		            }
		 
		            $data[] = $row;
		        }

		        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_admin->count_all($type),
                        "recordsFiltered" => $this->m_admin->count_filtered($type),
                        "data" => $data,
                );
        		break;
        	
        	default:
        		# code...
        		break;
        }
        //output to json format
        echo json_encode($output);
	}

	public function search() {
		var_dump(base_url());die();
	}

	public function api($type) {
		switch ($type) {
			case 'pegawaibynip':
				$link = 'http://opendatav2.tangerangkota.go.id/services/pegawai/pegawaibynip/nip/'.$_POST['nip'].'/format/json';
				$this->curl->create($link); 

				$this->curl->http_login(REST_U, REST_P); // login open data	
				$result = json_decode($this->curl->execute(), true);

				//var_dump($result);
				$data['nama']		= $result['nama_pegawai'].' '.$result['gelar_belakang'];
				$data['jabatan']	= $result['nomenklatur_jabatan'];
				$data['nama_unor']	= $result['nama_unor'];
				$data['nama_opd']	= $result['nomenklatur_pada'];
				echo json_encode($data);
				break;

			case 'SKPD':
				$link = 'http://opendatav2.tangerangkota.go.id/services/unor/unor_parent/format/json';
				$this->curl->create($link); 

				$this->curl->http_login(REST_U, REST_P); // login open data	
				$result = json_decode($this->curl->execute(), true);

				//var_dump(count($result),"<hr>",$result['0']);

				foreach ($result as $r) {
		  			$data['data'] 	= array('nama' => $r['nama_unor'], 'kode_unor' => $r['kode_unor'], 'status' => '1');
		  			$data['table']	= "SKPD";

		  			$this->m_admin->addData($data);
		  			var_dump("<hr>",$r['nama_unor']);
				}
				break;
			
			default:
				# code...
				break;
		}
	}

	public function nembak($type,$id=null) {
		switch ($type) {
			case 'user_akses_ktda':
				// die('masuk nembak');
				$data['sub_ktda']	= menu_all_sub_ktda();
				$data['table']		= "user_akses";
				foreach ($data['sub_ktda'] as $sk) {
					$data['data'] 	= array('ktda_id' => $sk->id, 'user_id' => $id, 'status' => '1');
			
					$this->m_admin->addData($data);
				}
				break;
			
			default:
				# code...
				break;
		}
	}
}
?>