<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function ExcelToJSON($path_file) {
	$CI = get_instance();
	$file = './assets/uploads/'.$path_file;
	//$file = "file:///E:/web_server/htdocs/latihan/CI3_latihan/assets/fiels/text.xlsx";
	//load the excel library
	//$this->load->library('excel');
	//read file from path
	$objPHPExcel = PHPExcel_IOFactory::load($file);
	//get only the Cell Collection
	$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
	//extract to a PHP readable array format
	$header = array();
	foreach ($cell_collection as $cell) {
	    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
	    if ($row == 1) {
	    	continue;
	    }
		$arr_data['baris'.$row] = array();
	}
	foreach ($cell_collection as $cell) {
	    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
	    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
	    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
	    //header will/should be in row 1 only. of course this can be modified to suit your need.
	    //var_dump("<hr> row dan data_value <br>",$row,$data_value);
	    if ($row == 1) {
	        // $header['baris'.$row][$column] = $data_value;
	        array_push($header, $data_value);
	    } else {
			// $arr_data['baris'.$row][$column] = array();
	        // $arr_data['baris'.$row][$column] = $data_value;
	        array_push($arr_data['baris'.$row], $data_value);
	    }
	}
	//send the data in an array format
	$data['header'] = $header;
	$data['values'] = $arr_data;

	// var_dump("<hr>Data ExcelToJson <br>",$data);
	// var_dump("<hr>json_encode data <br>",json_encode($data));
	// die();
	// return json_encode($data);
	return $data;
}

function Excel_Generator($value) {	
	$CI = get_instance();
	// $alphas = range('A', 'Z'); // array alfabet A - Z (26 data)
	$alphas = array();
	for ($i = 'A'; $i !== 'JK'; $i++){
	    array_push($alphas, $i);
	}

	$baris = count($value['json']['baris']); // jumlah baris + 1 (nama baris di A1)
	// var_dump("<hr>",$value['json']['baris']);
	// var_dump("<hr>",$alphas);

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

	//date_default_timezone_set('Europe/London');

	/** Include PHPExcel */
	require_once APPPATH."/third_party/PHPExcel.php";
	//require_once base_url(). 'application/libraries/PHPExcel.php';

	// Create new PHPExcel object
	// echo EOL;
	// echo date('H:i:s') , " Create new PHPExcel object" , EOL;
	$objPHPExcel = new PHPExcel();

	// Set document properties
	/*
	echo date('H:i:s') , " Set document properties" , EOL;
	$objPHPExcel->getProperties()->setCreator("Thoyib Hidayat")
								 ->setLastModifiedBy("Thoyib Hidayat")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");
	*/

	// Add some data
	echo date('H:i:s') , " Add some data" , EOL;
	// var_dump("<hr>",$value['json']['kolom'], EOL); //array nama kolom (A1,B1,C1,...)
	// var_dump("<hr>",$alphas['0']);die();
	$objPHPExcel->setActiveSheetIndex(0);
	for ($i=0; $i < $value['kolom'] ; $i++) { 
		for ($j=0; $j <= $baris ; $j++) { 
			if ($j == 0) { // jika baris pertama maka tampilkan kolom
				$objPHPExcel->getActiveSheet()->setCellValue($alphas[$i].($j+1), $value['json']['kolom'][$i]); // eksekusi per kolom
			} else {
				if ($i==0) { // jika kolom A maka tampilkan data baris
					$objPHPExcel->getActiveSheet()->setCellValue($alphas[$i].($j+1), $value['json']['baris'][$j-1]);
				}
			}
		}						
	}
	// Rename worksheet
	// echo date('H:i:s') , " Rename worksheet" , EOL;
	$objPHPExcel->getActiveSheet()->setTitle('Simple');


	// Set document security
	// echo date('H:i:s') , " Set cell protection" , EOL;


	// Set sheet security
	echo date('H:i:s') , " Set sheet security" , EOL;
	$range = 'B2:'.$alphas[$value['kolom']-1].($baris+1);
	$all_range = 'A1:'.$alphas[$value['kolom']-1].($baris+1);
	$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
	$objPHPExcel->getActiveSheet()
		->getStyle($range)
		->getProtection()->setLocked(
			PHPExcel_Style_Protection::PROTECTION_UNPROTECTED
		);
	$objPHPExcel->getActiveSheet()->getProtection()->setPassword("K0minfO");
		
	$styleArray = array(
	    'borders' => array(
	        'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN
	        )
	    )
	  );
	$objPHPExcel->getActiveSheet()->getStyle($all_range)->applyFromArray($styleArray);

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Save Excel 2007 file
	echo date('H:i:s') , " Write to Excel2007 format" , EOL;
	$callStartTime = microtime(true);
	$target = 'assets/downloads/files/';
	$name_file = strtoupper($value['judul']).'-'.time().'.xlsx';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

	// echo date('H:i:s') , " Moving file to another folder" , EOL;
	rename(str_replace('.php', '.xlsx', __FILE__), $target.$name_file);
	//$objWriter->save($target);
	$callEndTime = microtime(true);
	$callTime = $callEndTime - $callStartTime;

	return $name_file;
}

function RowToArray($baris) {
	$array = array();
	$huruf = range('A', 'Z');
	$i = 1;
	$CI = get_instance();
	$value['table_baris'] = $baris;
	//list($value['id_baris'],$value['table_baris'],$nama_baris) = explode("-", $baris);
	//var_dump("<hr>",$data['src_data']);
	//switch data baris
	switch ($value['table_baris']) {
		case 'd_sal_pembuang':
			$src_data['select']		= "*";
			$src_data['table']		= "d_das";
			$src_data['where'] 		= "status = 1 ";
			$data['das'] 			= $CI->m_ktda->getData($src_data);
			$c_das = count($data['das']);
			$counter = 0;

			foreach ($data['das'] as $das) {
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '0' AND das_id = '".$das->id."' ";
				$data['kali'] 			= $CI->m_ktda->getData($src_data);

				foreach ($data['kali'] as $kali) {
					array_push($array, $kali->nama);
					$src_data['select']		= "*";
					$src_data['table']		= $value['table_baris'];
					$src_data['where'] 		= "status = 1 AND parent_id = '".$kali->id."' ";
					$data['saluran'] 		= $CI->m_ktda->getData($src_data);
					$j = 1;
					foreach ($data['saluran'] as $sal) {
						array_push($array, $j.". ".$sal->nama);
						$j++;
					}
				}
				array_push($array, $das->nama);				
				$counter++;
				if ($c_das != $counter) {					
					array_push($array, " ");
				}
			}
			break;

		case 'd_lokasi_banjir':			
			$src_data['select']		= "l.*, k.nama as kelurahan, c.nama as kecamatan";
			$src_data['table']		= $value['table_baris'].' as l';
			$src_data['join']['0']	= array("d_kelurahan AS k","k.id = l.kelurahan_id");
			$src_data['join']['1']	= array("d_kecamatan AS c","c.id = k.kecamatan_id");
			$src_data['where'] 		= "l.status = 1 ";
			$src_data['order'] 		= "k.kecamatan_id ASC ";
			$data['src_data'] 		= $CI->m_ktda->getData($src_data);

			// kolom ke 2
			foreach ($data['src_data'] as $d) {				
				if (($d->nama == NULL) || ($d->nama == "")) {
					array_push($array, $d->kecamatan.' - Kel '.$d->kelurahan);
				} else {
					array_push($array, $d->kecamatan.' - '.$d->nama.' , Kel '.$d->kelurahan);
				}
			}
			break;

		case 'd_golongan':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 		= $CI->m_ktda->getData($src_data);
			$c_golongan = count($data['data1']);
			$counter = 0;

			foreach ($data['data1'] as $data1) {
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 		= $CI->m_ktda->getData($src_data);

				array_push($array, "GOLONGAN ".$data1->nama);	
				foreach ($data['data2'] as $data2) {
					array_push($array, $data1->nama.'/'.$data2->nama);
				}	

				$counter++;
				if ($c_golongan != $counter) { array_push($array, " "); }
			}
			break;

		case 'd_partai(fraksi)':
			$src_data['select']		= "*";
			$src_data['table']		= "d_partai";
			$src_data['where'] 		= "status = 1 ";
			$data['src_data'] 		= $CI->m_ktda->getData($src_data);

			foreach ($data['src_data'] as $d) {
				array_push($array, "FRAKSI ".$d->nama);
			}
			break;

		case 'd_kelurahan_kcda':
			$src_data['select']		= "*";
			$src_data['table']		= "d_kelurahan";
			$src_data['where'] 		= "status = 1 and kecamatan_id = '".$_SESSION['kecamatan_id']."' ";
			$data['src_data'] 		= $CI->m_ktda->getData($src_data);

			foreach ($data['src_data'] as $d) {
				array_push($array, $d->nama);
			}
			$CI->session->unset_userdata('kecamatan_id');
			break;

		//2 lvl (angka,huruf)
		case 'd_rekap_realisasi_pendapatan_dan_belanja_pemerintah':
		case 'd_realisasi_pendapatan':
		case 'd_realisasi_belanja':
		case 'd_agregat_pdrb':
		case 'd_jenis_simpanan_perbankan_syariah':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 			= $CI->m_ktda->getData($src_data);
			$c_ip = count($data['data1']);
			$counter1 = 0;

			foreach ($data['data1'] as $data1) {
				$counter2 = 0;
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 			= $CI->m_ktda->getData($src_data);
				array_push($array, ($counter1+1).". ".$data1->nama);	
				foreach ($data['data2'] as $data2) {
					array_push($array, $huruf[$counter2].". ".$data2->nama);
					$counter2++;
				}
				$counter1++;
				if ($c_ip != $counter1) { array_push($array, " "); }
			}
			break;

		//3 lvl (huruf,angka,angka)
		case 'd_realisasi_penerima_pajak':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 		= $CI->m_ktda->getData($src_data);
			$c_ip = count($data['data1']);
			$counter1 = 0;

			foreach ($data['data1'] as $data1) {
				$counter2 = 0;
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 		= $CI->m_ktda->getData($src_data);

				array_push($array, $huruf[$counter1].". ".$data1->nama);	
				foreach ($data['data2'] as $data2) {
					$counter3 = 0;
					$src_data['select']		= "*";
					$src_data['table']		= $value['table_baris'];
					$src_data['where'] 		= "status = 1 AND parent_id = '".$data2->id."' ";
					$data['data3'] 		= $CI->m_ktda->getData($src_data);

					array_push($array, ($counter2+1).". ".$data2->nama);
					foreach ($data['data3'] as $data3) {
						array_push($array, ($counter2+1).".".($counter3+1)." ".$data3->nama);
						$counter3++;
					}
					$counter2++;
				}
				$counter1++;
				if ($c_ip != $counter1) { array_push($array, " "); }
			}
			break;

		case 'd_perumahan':
			$src_data['select']		= "p.*, k.nama as kelurahan";
			$src_data['table']		= $value['table_baris'].' as p';
			$src_data['join']['0']	= array("d_kelurahan AS k","k.id = p.kelurahan_id");
			$src_data['where'] 		= "p.status = 1 ";
			$src_data['order'] 		= "p.kelurahan_id ASC ";
			$data['src_data'] 		= $CI->m_ktda->getData($src_data);

			// kolom ke 2
			foreach ($data['src_data'] as $d) {				
				array_push($array, $d->kelurahan.' - '.$d->nama);
			}
			break;

		//2 lvl (kosong,angka)
		case 'd_indikator_perumahan':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 			= $CI->m_ktda->getData($src_data);
			$c_ip = count($data['data1']);
			$counter1 = 0;

			foreach ($data['data1'] as $data1) {
				$counter2 = 0;
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 			= $CI->m_ktda->getData($src_data);
				array_push($array, $data1->nama);

				foreach ($data['data2'] as $data2) {
						array_push($array, ($counter2+1).". ".$data2->nama);
						$counter2++;
				}
				$counter1++;
				if ($c_ip != $counter1) { array_push($array, " "); }	
			}
			break;

		//3 lvl (kosong,angka,huruf)
		case 'd_layanan_ipal-iplt-iplc':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 			= $CI->m_ktda->getData($src_data);
			$c_ip = count($data['data1']);
			$counter1 = 0;

			foreach ($data['data1'] as $data1) {
				$counter2 = 0;
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 			= $CI->m_ktda->getData($src_data);
				array_push($array, $data1->nama);

				foreach ($data['data2'] as $data2) {
					$counter3 = 0;
					$src_data['select']		= "*";
					$src_data['table']		= $value['table_baris'];
					$src_data['where'] 		= "status = 1 AND parent_id = '".$data2->id."' ";
					$data['data3'] 			= $CI->m_ktda->getData($src_data);
					array_push($array, ($counter2+1).". ".$data2->nama);
					$counter2++;

					foreach ($data['data3'] as $data3) {
						array_push($array, $huruf[$counter3].".) ".$data2->nama);
						$counter3++;
					}
				}
				$counter1++;
				if ($c_ip != $counter1) { array_push($array, " "); }	
			}
			break;

		//2 lvl (huruf,angka)
		case 'd_jenis_surat_pos':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 		= $CI->m_ktda->getData($src_data);
			$c_ip = count($data['data1']);
			$counter1 = 0;

			foreach ($data['data1'] as $data1) {
				$counter2 = 0;
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 		= $CI->m_ktda->getData($src_data);
				array_push($array, $huruf[$counter1].". ".$data1->nama);
				foreach ($data['data2'] as $data2) {
						array_push($array, ($counter2+1).". ".$data2->nama);
						$counter2++;
				}
				$counter1++;
				if ($c_ip != $counter1) { array_push($array, " "); }	
			}
			break;
		
		//2lvl (kosong,huruf)
		case 'd_kabupaten_kota_banten':
		case 'd_jenis_dana_perbankan':
		case 'd_jenis_tarif_pam':
		case 'd_tanaman_pangan':
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 and parent_id = '0' ";
			$data['data1'] 		= $CI->m_ktda->getData($src_data);
			$c_ip = count($data['data1']);
			$counter1 = 0;

			foreach ($data['data1'] as $data1) {
				$counter2 = 0;
				$src_data['select']		= "*";
				$src_data['table']		= $value['table_baris'];
				$src_data['where'] 		= "status = 1 AND parent_id = '".$data1->id."' ";
				$data['data2'] 		= $CI->m_ktda->getData($src_data);
				array_push($array, $data1->nama);
				foreach ($data['data2'] as $data2) {
						array_push($array, $huruf[$counter2].". ".$data2->nama);
						$counter2++;
				}
				$counter1++;
				if ($c_ip != $counter1) { array_push($array, " "); }	
			}
			break;

		default:
			$src_data['select']		= "*";
			$src_data['table']		= $value['table_baris'];
			$src_data['where'] 		= "status = 1 ";
			$data['src_data'] 		= $CI->m_ktda->getData($src_data);

			foreach ($data['src_data'] as $d) {
				array_push($array, $d->nama);
			}
			break;
	}
	//return $json_baris;
	var_dump("<hr>RowToArray",$array);
	return $array;
}
?>