<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');



function random($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function date_timestamp() {
	$date = date('Y-m-d H:i:s');
	return $date;
}



function email($value) {
	$data['email']			= $value['email'];
	$data['token']			= $value['token'];
	$data['name'] 			= $value['user'];
	$data['subject'] 		= 'Lupa Password SISTAD';
	$data['message'] 		= 'Klik link dibawah ini untuk mereset password anda. <br> '.base_url().'admin/forget/'.$value['token'];
	$data['created_date'] 	= $value['date'];

	$CI = get_instance();
    $CI->load->model('m_admin');
    $CI->m_admin->addMail($data);

	$CI->email->from('thoyib07@gmail.com'); //replace to in email configuration file
	$CI->email->to($data['email']);
	$CI->email->reply_to('thoyib07@gmail.com'); //replace to in email configuration file
	$CI->email->subject($data['subject']);
	$CI->email->message($data['message']);
	var_dump($CI->email->item('smtp_user','email'));die();

	//$CI->email->send();
	redirect(base_url().'admin/');

}



function active_time() {
	$timer = 60 * 60; //60 menit
	$CI = get_instance();

	if ($CI->session->userdata('cms_login') != 1) {
		redirect('auth/login');
	} elseif (time() - $CI->session->userdata('cms_time') > $timer) {
		$session = array('cms_login' => FALSE, 'cms_time' => '');
		// $session = array('login' => true, 'time' => time(), 'id_user' => '1');
		$CI->session->set_userdata($session);
		redirect('admin/index');
	}
}

function info_user() {
	// var_dump($_SESSION);die();
	$CI = get_instance();
	$user['select']		= "u.id_user,u.username";
	$user['table']		= "m_user as u";
	// $user['join'][0]	= array('m_user_type as t', 't.id_user_type = u.id_user_type' );
	$user['where'] 		= "u.status = 1 and u.id_user = '".$_SESSION['cms_id_user']."'";
	// $user['where'] 		= "u.status = 1 and u.id_user = '1'";
	$user['limit']		= "limit 1";
	$data['user'] 		= $CI->m_admin->getAll($user);

	return $data['user'];
}



function list_table() {	 
	$CI = get_instance();
	$tables=$CI->db->query("SHOW TABLES FROM `db_lms_dirga` LIKE 'd_%'")->result_array(); 
	var_dump($tables);
}



function isSuper() {
	$CI = get_instance();
	if ($CI->session->userdata('cms_akses') == "1") {
		return TRUE;
	} else {
		return FALSE;
	}
}



function isAdmin() {
	$CI = get_instance();
	if ($CI->session->userdata('cms_akses') == "2") {
		return TRUE;
	} else {
		return FALSE;
	}
}



function isOperator() {
	$CI = get_instance();
	if ($CI->session->userdata('cms_akses') == "3") {
		return TRUE;
	} else {
		return FALSE;
	}
}

// function cek_akses_admin() {
// 	switch ($_SERVER['HTTP_HOST']) {
// 		case 'ikmdukcapilluwutimurkab.com':
// 		case 'www.ikmdukcapilluwutimurkab.com':			
// 			return TRUE;
// 		break;		

// 		default:
// 			redirect(base_url());
// 		break;
// 	}
// }



/* End of file upload_file_helper.php */