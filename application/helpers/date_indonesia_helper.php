<?php // test_helper.php
if(!defined('BASEPATH')) exit('No direct script access allowed');

function date_indonesia($format = 'd F, Y',$timestamp = NULL)
{
    $l = array('', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu');
    $F = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    $return = '';
    if(is_null($timestamp)) { $timestamp = mktime(); }
    for($i = 0, $len = strlen($format); $i < $len; $i++) {
        switch($format[$i]) {
            case '\\' :
                $i++;
                $return .= isset($format[$i]) ? $format[$i] : '';
                break;
            case 'l' :
                $return .= $l[date('N', $timestamp)];
                break;
            case 'F' :
                $return .= $F[date('n', $timestamp)];
                break;
            default :
                $return .= date($format[$i], $timestamp);
                break;
        }
    }
    return $return;
}

?>