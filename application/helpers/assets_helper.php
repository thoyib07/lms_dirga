<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	function load_css($files, $base_url) {
		foreach ($files as $file) 
		{
			echo '<link href="'.$base_url.'/assets/frontend/plugins/'.$file.'" rel="stylesheet" type="text/css" />';
		}
	}
	
	
	function admin_load_css($files, $base_url) {
		foreach ($files as $file) 
		{
			echo '<link href="'.$base_url.'/assets/admin/'.$file.'" rel="stylesheet" type="text/css" />';
		}
	}
	
	function admin_load_js($files, $base_url) {
		foreach ($files as $file) 
		{
			echo '<script src="'.$base_url.'/assets/admin/'.$file.'" type="text/javascript"></script>';
		}
	}

	function load_js($files, $base_url) {
	    foreach ($files as $file) 
		{
			echo '<script src="'.$base_url.'/assets/frontend/plugins/'.$file.'" type="text/javascript"></script>';
		}
	}

	function load_meta($meta) {
		foreach($meta as $name => $value)
		{
			echo '<meta name="'.$name.'" content="'.$value.'">';
		}
	}

/* End of file asset_helper.php */